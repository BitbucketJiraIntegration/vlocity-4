global with sharing class PaymentCreation implements vlocity_cmt.VlocityOpenInterface
{
    

    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean result = true;
        try
        {
            if (methodName.equals('GetPaymentData'))
            {
                GetPaymentData(inputMap,outMap);
            }
            else 
            {
                result = false;
            }   
        } 
        catch(Exception e)
        {
            System.debug('PaymentCreation:invokeMethod -> exception: '+e);
            result = false;
        }
        
        return result;
    }
    
void GetPaymentData(Map<String, Object> inputMap, Map<String, Object> outMap)
   {
       System.debug('@@@@ inputMap : ' + inputMap);
        String sAccountId = (String)inputMap.get('AccountId');
        System.debug('Id Entered: ' + sAccountId);
        Set<Id> serAgreementId = new Set<Id>();
        List<Service_Point__c> serPointPremisesList = [Select Service_Agreement__r.Id, Service_Agreement__r.SA_Type__c,Service_Agreement__r.Start_Option__c, Service_Agreement__r.SA_Status_Flag__c, Service_Agreement__r.Start_Date__c, Service_Agreement__r.Service_Agreement_ID__c, Id, Premises__r.Id, Premises__r.vlocity_cmt__State__c, Premises__r.vlocity_cmt__City__c, Premises__r.vlocity_cmt__PostalCode__c from Service_Point__c where Service_Agreement__r.Account__c = :sAccountId];
        List<Map<String, String>>  returnList = new List<Map<String,String>>();
        Map<String, String> PremisesList = null;
        if(serPointPremisesList.size() > 0) {
            
            for(Service_Point__c sp : serPointPremisesList) {
                PremisesList = new Map<String, String>();
                PremisesList.put('SA_ID', sp.Service_Agreement__r.Id);
                PremisesList.put('SA_Type', sp.Service_Agreement__r.SA_Type__c);
                PremisesList.put('Start_Option', sp.Service_Agreement__r.Start_Option__c);
                PremisesList.put('SA_Status_Flag', sp.Service_Agreement__r.SA_Status_Flag__c);
                PremisesList.put('Service_Agreement_ID', sp.Service_Agreement__r.Service_Agreement_ID__c);
                PremisesList.put('Start_Date', sp.Service_Agreement__r.Start_Date__c.format()); 
                PremisesList.put('PremisesId', sp.premises__r.Id);
                PremisesList.put('PremisesState', sp.premises__r.vlocity_cmt__State__c);
                PremisesList.put('PremisesCity', sp.premises__r.vlocity_cmt__City__c);
                PremisesList.put('PremisesPostalCode', sp.premises__r.vlocity_cmt__PostalCode__c);
                returnList.add(PremisesList);
            }            
           outMap.put('PremisesList', returnList);
            //outMap.put('payment-creation-step', '{ \'PaymentSegments\' : {\'PremisesList\' : [{\'Id\' : \'abc\'},{\'Id\' : \'PQR\'}] } }');
            //outMap.put('payment-creation-step1', new Map<String, Object> {'paymentSegment' => new Map<String, Object> {'PremisesList' => returnList } });
            System.debug('outMap--->'+outMap);
        } else{
           outMap.put('PaymentList', 'NOT FOUND');
        }
    }        
}