global with sharing class CCB_ILServiceAggrement implements vlocity_cmt.VlocityOpenInterface2
{  

    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
        {
           
            //system.debug('#####'+searchList);
            List<String> contextIds = (List<String>) inputs.get('contextIds');
            String parentId = (String) inputs.get('parentId');           
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');            
            String lastRecordId = request.lastRecordId;   // R15 pagination support            
            //get values entered by the user           
            Map<String,Object> searchValueMap = request.searchValueMap;            
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');            
            Integer pageSize = 0; // R15 pagination support           
            Integer numRecords = 30; // R15 pagination support
            system.debug('@@@@@@@@'+request.searchValueMap);
            String searchedParameter = '';
            if(!searchValueMap.isEmpty()){
                for(String st:searchValueMap.keySet()){
                	searchedParameter =  st;   
                }            	   
            } system.debug('!!!!!'+searchedParameter);
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support
            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');
            
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;
            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('AccountId');
            resultFieldList.add('PersonId');
            resultFieldList.add('PersonName');
            //resultFieldList.add('BusinessName');
            resultFieldList.add('RelationshipType');
           	resultFieldList.add('Phone');// couldn't get the field 	Primary Phone Number (Phone Type, Phone Number) so taken Phone field
            //resultFieldList.add('vlocity_cmt__DriversLicenseNumber__c');//this is written inside check condition of record type
            //resultFieldList.add('vlocity_cmt__TaxId__c');//this is written inside check condition of record type
            //resultFieldList.add('ServiceAggrementId');
            //resultFieldList.add('MeterNumber');
            //resultFieldList.add('Primises_Address__c'); 
             resultFieldList.add('Primises');
             List<String> verificationFieldList = new List<String> ();
            //verificationFieldList.add('PersonName'); 
            //verificationFieldList.add('AccountId');
            
            List<String> optionalVerificationFieldList = new List<String> ();
            //optionalVerificationFieldList.add('PersonId');            
           
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();            
            Map<String, String> newType1 = new Map<String, String> ();            
    
            newType1 = new Map<String, String> ();            
            newType1.put('label','Account Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Person Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Name');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonName', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Account Relationship Type');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('RelationshipType', newType1);
           
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Phone');
            newType1.put('datatype','Phone');            
            resultFieldsLabelTypeMap.put('Phone', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Primises');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Primises', newType1);
            
            //below code is shifted inside recordtype check    
            /*newType1 = new Map<String, String> ();
            newType1.put('label', 'Driver\'s License Number');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'TIN');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__TaxId__c', newType1);
 			
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Primises');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Primises_Address__c', newType1);*/
            
            
            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            String drBundleName = (String) resultInfo.get('drBundleName');
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap = new Map<String, Object>(); 
              }
               // R15 pagination support
            else{
                String query ='';
                String whereClause = '';
                String searchedValue = '';
                Map<String,List<String>> AccountIdMap = New Map<String, List<String>>();                
                Boolean dynamicPersonLableAdded = false;
                Boolean dynamicBusinessLableAdded = false;
                Map<String,List<Service_Agreement__c>> AccountServiceAgrMap = New Map<String,List<Service_Agreement__c>>();
                Map<String,Service_Point__c> ServicePointAccountMap = New Map<String, Service_Point__c>();
                Map<ID, vlocity_cmt__Premises__c> premisesMap = new Map<ID, vlocity_cmt__Premises__c>();
                Set<Id> SAIds = new Set<Id>();     
                List<AccountContactRelation> data = null;
                
                if(searchValueMap != null && !searchValueMap.isEmpty()) {
                    
                    if(searchedParameter=='Service_Agreement_ID__c'){
                        List<List<Service_Agreement__c>> searchList = New List<List<Service_Agreement__c>>();
                        resultFieldList.add('ServiceAggrementId');                               
                        newType1 = new Map<String, String> ();
                        newType1.put('label', 'Service Aggrement ID');
                        newType1.put('datatype','text');            
                        resultFieldsLabelTypeMap.put('ServiceAggrementId', newType1);
                        for(String field : searchValueMap.keySet()){
                            if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                        		searchedValue = String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)));
                            }
                        }
                        
                        if(String.isNotBlank(searchedValue)){                             
                             String searchValueFind = searchedValue+'*';                                
                        	 searchList = [FIND :searchValueFind IN ALL FIELDS RETURNING 
                                                 Service_Agreement__c  (Characteristic_Premise_ID__c,Service_Agreement_ID__c, Account__c , Id WHERE Service_Agreement_ID__c LIKE :'%'+searchedValue+'%')];	    
                        }
                        for(List<Service_Agreement__c> sl:searchList){                       	
                            for(Service_Agreement__c insideList:sl){
                                SAIds.add(insideList.Id);
                                if(AccountServiceAgrMap.containsKey(insideList.Account__c)) {
                                    List<Service_Agreement__c> serviceAgreements = AccountServiceAgrMap.get(insideList.Account__c);
                                    serviceAgreements.add(insideList);
                                    AccountServiceAgrMap.put(insideList.Account__c,serviceAgreements);
                                } else {
                                     AccountServiceAgrMap.put(insideList.Account__c, new List<Service_Agreement__c> { insideList });
                                }
                            }
                           
                        }system.debug('1$$$$$$1'+AccountServiceAgrMap);
                        if(!AccountServiceAgrMap.isEmpty()){
                            List<Account> Accounts = [Select Id,Account_ID__c from Account WHERE ID IN : AccountServiceAgrMap.keySet()]; 
                            String InQueryClause = '';
                            List<Service_Agreement__c> SAs;
                            for(Account acc:Accounts){
                                SAs =  New List<Service_Agreement__c>();
                                SAs.addAll(AccountServiceAgrMap.get(acc.Id));
                                AccountServiceAgrMap.remove(acc.Id);
                                AccountServiceAgrMap.put(acc.Account_ID__c,SAs);
                                
                                if(String.isNotEmpty(InQueryClause)){
                                 	InQueryClause = InQueryClause+'\''+acc.Account_ID__c+'\',';    
                                 }
                                 else{
                                 	InQueryClause = '('+'\''+acc.Account_ID__c+'\',';    
                                 }
                            		    
                            }
                            String AccountContactRelationQuery ='';
                            if(String.isNotBlank(InQueryClause)){
                                InQueryClause = InQueryClause.removeEnd(',');
                                InQueryClause = InQueryClause+')';	    
                        	}
                            if(String.isNotBlank(InQueryClause)){
                                AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c, Contact.Phone,  Contact.vlocity_cmt__DriversLicenseNumber__c, Contact.recordtype.name,Contact.vlocity_cmt__TaxId__c, Account.Customer_Class__c, Account.Customer_Class_Code__c FROM AccountContactRelation WHERE MAIN_CUST_SW__c = true AND Account.Account_ID__c IN '+ InQueryClause ;
                                system.debug('8888888'+AccountContactRelationQuery);
                                data = Database.query(AccountContactRelationQuery);                       		    
                            }system.debug('aaaaaaa'+AccountContactRelationQuery);
                          system.debug('9999999'+data);
                        }
                        system.debug('1%%%%%1'+data);
                        
                       //data for SA
                       if(!data.isEmpty()) {
                           //query on service points if data from AccountContactRelation is not null;
                           List<Service_Point__c> serviceAgreementsPoints = New List<Service_Point__c>();
                           Map<String, String> AgrPremiseMap = New Map<String, String>();
                           
                           serviceAgreementsPoints = [Select Id, Service_Agreement__r.Service_Agreement_ID__c, Premises__r.Consolidated_Address__c from Service_Point__c Where Service_Agreement__c IN : SAIds];
                           for(Service_Point__c sp :serviceAgreementsPoints){
                           		AgrPremiseMap.put(sp.Service_Agreement__r.Service_Agreement_ID__c,sp.Premises__r.Consolidated_Address__c);    
                           }    
                           List<Service_Agreement__c> serAgrLst = New List<Service_Agreement__c>();
                           for(AccountContactRelation con : data) {
                               if(AccountServiceAgrMap.containsKey(con.Account.Account_ID__c)){
                               	serAgrLst =  AccountServiceAgrMap.get(con.Account.Account_ID__c);
                                   for(Service_Agreement__c srt:serAgrLst){
                                   		
                                         Map<String, object> resultValueMap = new Map<String, object>();
                                         resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');
                                         resultValueMap.put('PersonId',con.Contact.Person_ID__c != null ? con.Contact.Person_ID__c : '');
                                         resultValueMap.put('PersonName',con.Contact.Name != null ? con.Contact.Name : '');
                                       	 if(AgrPremiseMap.containsKey(srt.Service_Agreement_ID__c)){
                                         	resultValueMap.put('Primises', AgrPremiseMap.get(srt.Service_Agreement_ID__c) != null ? AgrPremiseMap.get(srt.Service_Agreement_ID__c) : '');	   
                                       	 }
                                         
                                        resultValueMap.put('ServiceAggrementId',srt.Service_Agreement_ID__c != null ? srt.Service_Agreement_ID__c : '');
                                       	if('RES'.equalsIgnoreCase(con.Account.Customer_Class_Code__c.trim())){
                                               resultValueMap.put('RelationshipType','Residential');
                                                if(!dynamicPersonLableAdded){
                                                    resultFieldList.add('vlocity_cmt__DriversLicenseNumber__c');
                                                    newType1 = new Map<String, String> ();
                                                    newType1.put('label', 'Driver\'s License Number');
                                                    newType1.put('datatype','text');            
                                                    resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
                                                    dynamicPersonLableAdded = true;
                                                }
                                                resultValueMap.put('vlocity_cmt__TaxId__c',''); 
                                                resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c',con.Contact.vlocity_cmt__DriversLicenseNumber__c != null ? con.Contact.vlocity_cmt__DriversLicenseNumber__c : '');    
                                            }
                                          else {
                                                if(!'RES'.equalsIgnoreCase(con.Account.Customer_Class_Code__c.trim()) && String.isNotBlank(con.Account.Customer_Class_Code__c)){
                                                    resultValueMap.put('RelationshipType','Non-Residential' );
                                                }
                                                else{
                                                    resultValueMap.put('RelationshipType','' );     
                                                }
                                                if(!dynamicBusinessLableAdded){
                                                    resultFieldList.add('vlocity_cmt__TaxId__c');                               
                                                    newType1 = new Map<String, String> ();
                                                    newType1.put('label', 'TIN');
                                                    newType1.put('datatype','text');            
                                                    resultFieldsLabelTypeMap.put('vlocity_cmt__TaxId__c', newType1);
                                                    dynamicBusinessLableAdded = true;
                                                }
                                                resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c','');   
                                                resultValueMap.put('vlocity_cmt__TaxId__c',con.Contact.vlocity_cmt__TaxId__c != null ? con.Contact.vlocity_cmt__TaxId__c : '');    
                                            } 
											String recordId = con.Contact.Id;  
                                            vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);
                                            result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                                            results.add(result);
                                   }
                               }		    
                           }
                       } 
                    }
                    
                    System.debug('00000'+searchedParameter);
                    if(searchedParameter=='Meter_ID__c'){
                        Service_Point__c servPoint = new Service_Point__c();
                        AccountServiceAgrMap = New Map<String,List<Service_Agreement__c>>();
                        List<List<Service_Point__c>> searchList = New List<List<Service_Point__c>>();                        
                        List<Service_Agreement__c> SAList = New List<Service_Agreement__c>();
                        Map<Id, vlocity_cmt__Premises__c> SApremiseMap = New Map<Id, vlocity_cmt__Premises__c>();
                        resultFieldList.add('MeterNumber');
                        
                        newType1 = new Map<String, String> ();
                        newType1.put('label', 'Meter ID');
                        newType1.put('datatype','text');            
                        resultFieldsLabelTypeMap.put('MeterNumber', newType1);
                         
                        
                        Map<Id, Id> premiseIdMap = New Map<Id, Id>();
                        for(String field : searchValueMap.keySet()){
                            if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                        		searchedValue = String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)));
                            }
                        }
                        
                        if(String.isNotBlank(searchedValue)){   
                             String searchValueFind = searchedValue+'*';                                
                        	 searchList = [FIND :searchValueFind IN ALL FIELDS RETURNING 
                                                 Service_Point__c  (Premises__c, Service_Agreement__c, Service_Point_ID__c, Meter_ID__c, Id WHERE Meter_ID__c LIKE :'%'+searchedValue+'%')];	    
                        }
                        
                        for(List<Service_Point__c> sl:searchList){             	
                            for(Service_Point__c insideList:sl){System.debug('insideList!!!'+insideList);
                                premiseIdMap.put(insideList.Premises__c,insideList.Id);
                                ServicePointAccountMap.put(insideList.Service_Agreement__c,insideList);
                            }    
                        }
                        System.debug(premiseIdMap.keySet()+'22222'+ServicePointAccountMap);
                        premisesMap = new Map<ID, vlocity_cmt__Premises__c>([SELECT Id, Mailing_Address__c, Consolidated_Address__c FROM vlocity_cmt__Premises__c WHERE ID IN:premiseIdMap.keySet()]);
                        
                        for(vlocity_cmt__Premises__c prem:premisesMap.values()){
                        	SApremiseMap.put(premiseIdMap.get(prem.Id),prem);	    
                        }
                        if(!ServicePointAccountMap.isEmpty()){
                            String InQueryClauseSA = '';
                            String SARecordMainQuery = '';
                            for(String st:ServicePointAccountMap.keySet()){
                            	if(String.isNotEmpty(InQueryClauseSA)){
                                 	InQueryClauseSA = InQueryClauseSA+'\''+st+'\',';    
                                }
                                else{
                                    InQueryClauseSA = '('+'\''+st+'\',';    
                                }    
                            }
                            if(String.isNotBlank(InQueryClauseSA)){
                                InQueryClauseSA = InQueryClauseSA.removeEnd(',');
                                InQueryClauseSA = InQueryClauseSA+')';	    
                        	}
                            if(String.isNotBlank(InQueryClauseSA)){
                                SARecordMainQuery = 'SELECT Id, Service_Agreement_ID__c, Account__r.Account_ID__c FROM Service_Agreement__c Where ID IN '+ InQueryClauseSA ;
                                System.debug('33333'+SARecordMainQuery);
                                SAList = Database.query(SARecordMainQuery);
                            }
                        		    
                        }
                        
                        system.debug('qqqqqqqq'+SAList);
                        Service_Point__c serviceAgreementsPoints = New Service_Point__c();
                        for(Service_Agreement__c sa: SAList){
                        	if(AccountServiceAgrMap.containsKey(sa.Account__r.Account_ID__c)) {
                                    List<Service_Agreement__c> serviceAgreements = AccountServiceAgrMap.get(sa.Account__r.Account_ID__c);
                                    serviceAgreements.add(sa);
                                    AccountServiceAgrMap.put(sa.Account__r.Account_ID__c, serviceAgreements);
                             } else {
                                    AccountServiceAgrMap.put(sa.Account__r.Account_ID__c, new List<Service_Agreement__c> { sa });
                            }
                            system.debug(ServicePointAccountMap.keySet()+'aaaaaaa'+sa.Id);
                            if(ServicePointAccountMap.containsKey(sa.Id)) {
                                    serviceAgreementsPoints = New Service_Point__c(); 
                                    serviceAgreementsPoints = ServicePointAccountMap.get(sa.Id);system.debug('aaaserviceAgreementsPointsaaaa'+serviceAgreementsPoints);
                                    ServicePointAccountMap.remove(sa.Id);
                                    ServicePointAccountMap.put(sa.Service_Agreement_ID__c, serviceAgreementsPoints);
                             }		    
                        }
                        system.debug('777777777'+ServicePointAccountMap);
                        if(!AccountServiceAgrMap.isEmpty()){
                            String InQueryClauseAccContRel = '';
                            for(String acc:AccountServiceAgrMap.KeySet()){  
                                if(String.isNotEmpty(InQueryClauseAccContRel)){
                                 	InQueryClauseAccContRel = InQueryClauseAccContRel+'\''+acc+'\',';    
                                 }
                                 else{
                                 	InQueryClauseAccContRel = '('+'\''+acc+'\',';    
                                 }
                            		    
                            }
                            String AccountContactRelationQuery ='';
                            if(String.isNotBlank(InQueryClauseAccContRel)){
                                InQueryClauseAccContRel = InQueryClauseAccContRel.removeEnd(',');
                                InQueryClauseAccContRel = InQueryClauseAccContRel+')';	    
                        	}
                            if(String.isNotBlank(InQueryClauseAccContRel)){
                                AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c, Contact.Phone,  Contact.vlocity_cmt__DriversLicenseNumber__c, Contact.recordtype.name,Contact.vlocity_cmt__TaxId__c, Account.Customer_Class__c, Account.Customer_Class_Code__c FROM AccountContactRelation WHERE MAIN_CUST_SW__c = true AND Account.Account_ID__c IN '+ InQueryClauseAccContRel ;
                                System.debug('44444'+AccountContactRelationQuery);
                                data = Database.query(AccountContactRelationQuery);                       		    
                            }
                        }
                        
                        //data for Meter
                        if(!data.isEmpty() && searchedParameter=='Meter_ID__c') {
                           Map<String, String> AgrPremiseMapMeter = New Map<String, String>();
                           List<Service_Agreement__c> serAgrLst = New List<Service_Agreement__c>();
                            System.debug('Sobjet1==='+ data);
                            for(AccountContactRelation con : data) { 
                                System.debug('Sobjet==='+ con);
                                if(AccountServiceAgrMap.containsKey(con.Account.Account_ID__c)){  
                                    serAgrLst = AccountServiceAgrMap.get(con.Account.Account_ID__c);System.debug('st==='+ serAgrLst);
                                    System.debug('Sobjet2==='+ serAgrLst);
                                    for(Service_Agreement__c srt: serAgrLst){ 
                                         System.debug('Sobjet3==='+ srt);
                                        //serLst = ServicePointAccountMap.get(srt.Service_Agreement_ID__c);System.debug('serLst==='+ serLst);
                                            //for(Service_Point__c sp:serLst){
                                            //System.debug('sp==='+ sp);                                                  
                                            Map<String, object> resultValueMap = new Map<String, object>();
                                            resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');
                                            resultValueMap.put('PersonId',con.Contact.Person_ID__c != null ? con.Contact.Person_ID__c : '');
                                            resultValueMap.put('PersonName',con.Contact.Name != null ? con.Contact.Name : '');
											if(ServicePointAccountMap.containsKey(srt.Service_Agreement_ID__c)){
                                        		servPoint = ServicePointAccountMap.get(srt.Service_Agreement_ID__c); 
                                            	resultValueMap.put('MeterNumber',servPoint.Meter_ID__c != null ? servPoint.Meter_ID__c : '');
                                        	}
                                            if(SApremiseMap.containsKey(servPoint.Id)){
                                         		resultValueMap.put('Primises', SApremiseMap.get(servPoint.Id).Consolidated_Address__c != null ? SApremiseMap.get(servPoint.Id).Consolidated_Address__c : '');	   
                                       	 	}
                                        	if('RES'.equalsIgnoreCase(con.Account.Customer_Class_Code__c.trim())){
                                               resultValueMap.put('RelationshipType','Residential');
                                                if(!dynamicPersonLableAdded){
                                                    resultFieldList.add('vlocity_cmt__DriversLicenseNumber__c');
                                                    newType1 = new Map<String, String> ();
                                                    newType1.put('label', 'Driver\'s License Number');
                                                    newType1.put('datatype','text');            
                                                    resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
                                                    dynamicPersonLableAdded = true;
                                                }
                                                resultValueMap.put('vlocity_cmt__TaxId__c',''); 
                                                resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c',con.Contact.vlocity_cmt__DriversLicenseNumber__c != null ? con.Contact.vlocity_cmt__DriversLicenseNumber__c : '');    
                                            }
                                             else {
                                                if(!'RES'.equalsIgnoreCase(con.Account.Customer_Class_Code__c.trim()) && String.isNotBlank(con.Account.Customer_Class_Code__c)){
                                                    resultValueMap.put('RelationshipType','Non-Residential' );
                                                }
                                                else{
                                                    resultValueMap.put('RelationshipType','' );     
                                                }
                                                if(!dynamicBusinessLableAdded){
                                                    resultFieldList.add('vlocity_cmt__TaxId__c');                               
                                                    newType1 = new Map<String, String> ();
                                                    newType1.put('label', 'TIN');
                                                    newType1.put('datatype','text');            
                                                    resultFieldsLabelTypeMap.put('vlocity_cmt__TaxId__c', newType1);
                                                    dynamicBusinessLableAdded = true;
                                                }
                                                resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c','');   
                                                resultValueMap.put('vlocity_cmt__TaxId__c',con.Contact.vlocity_cmt__TaxId__c != null ? con.Contact.vlocity_cmt__TaxId__c : '');    
                                            } 
                                            
                                            
                                            String recordId = con.Contact.Id;  
                                            vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);
                                            result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                                            results.add(result);
                                          //}
                                    }
                                //}
                                }
                            }
                       }
                    }
                }
                
               //data removed from this position
                
            }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
                List<vlocity_cmt.LookupResult> resultSubSet = new List<vlocity_cmt.LookupResult>();
                // results.sort();                
                Integer startIndex = 0;           
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                      {
                            if(results[i].recordId == lastRecordId)
                            {
                                  startIndex = i + 1;
                            }
                        }
                }               
                Integer j = 0;            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }
            
                output.put('lookupResults', resultSubset);
                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
               output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
    global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {            
               vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest'); 
                List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
                Map<String, Object> additionalData = request.additionalData;     
                List<String> searchFieldList = request.searchFieldList;
                searchFieldList.add('Service_Agreement_ID__c');
                searchFieldList.add('Meter_ID__c');

                Map<String, Map<String, String>>labelTypeMap =   request.searchFieldsLabelTypeMap;

             	// newType2
                Map<String, String> newType2 = new Map<String, String> ();
                newType2.put('label','Service Agreement');
                newType2.put('datatype','text');
                labelTypeMap.put('Service_Agreement_ID__c',newType2);
        
        		Map<String, String> newType3  = new Map<String, String> ();
        		newType3.put('label','Meter Number');
                newType3.put('datatype','text');
                labelTypeMap.put('Meter_ID__c',newType3);
        
                Map<String, Object> valueMap = request.searchValueMap;

                System.debug(' LookupRequest is '+request); 
                output.put('lookupRequest', request);
       
    }
    
    global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);
        }       
        else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
        Boolean success = true;
        return success;
    }
}