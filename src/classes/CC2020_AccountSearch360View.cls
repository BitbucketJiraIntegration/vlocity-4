global with sharing class CC2020_AccountSearch360View implements vlocity_cmt.VlocityOpenInterface{
    
     global CC2020_AccountSearch360View() {}
    
     global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean result = true;
         System.debug('111111 ');
        try
        {
            if (methodName.equals('profileInforAndRelatedAccountJSON'))
            {	System.debug('2222222222222222 ');
                profileInforAndRelatedAccountJSON(inputMap,outMap);
            }
            else 
            {System.debug('3333333333333 ');
                result = false;
            }   
        } 
        catch(Exception e)
        {
            System.debug('QueryCases:invokeMethod -> exception: '+e);
            result = false;
        }
        
        return result;
    }
    
    public void profileInforAndRelatedAccountJSON(Map<String, Object> inputMap, Map<String, Object> outMap){
        System.debug('0000 '+inputMap.values());
        Id InteractionId = (String)inputMap.values()[1]; System.debug('rrrrrr '+inputMap.values()[1]);
    	String returnJSONString = '';
        Boolean businessFlag = false;
        vlocity_cmt__CustomerInteraction__c CustomerInteraction = NULL;
        Map<Id,Account> accData = New Map<Id,Account>();
        Map<Id,AccountContactRelation> accConRelData = New Map<Id,AccountContactRelation>();
        List<vlocity_cmt__ServicePoint__c> spData = new  List<vlocity_cmt__ServicePoint__c>();
        Map<Id,List<vlocity_cmt__ServicePoint__c>> spDataMap = new  Map<Id,List<vlocity_cmt__ServicePoint__c>>();
        List<vlocity_cmt__ServicePoint__c> spDataForACc = new  List<vlocity_cmt__ServicePoint__c>();
        String query;
        //Account profileAccountAccount = NULL;
        JSONGenerator gen = JSON.createGenerator(true);
        if(InteractionId!=NULL){ System.debug('QQQQQQQ '+InteractionId);
        	CustomerInteraction = [Select vlocity_cmt__AccountId__c, vlocity_cmt__AccountId__r.Phone, 
                                   vlocity_cmt__AccountId__r.Name,vlocity_cmt__AccountId__r.Account_ID__c,                                                                    
                                   vlocity_cmt__AccountId__r.Email__c, vlocity_cmt__AccountId__r.vlocity_cmt__TaxID__c,
                                   vlocity_cmt__ContactId__c , vlocity_cmt__ContactId__r.Name, 
                                   vlocity_cmt__ContactId__r.Email, vlocity_cmt__ContactId__r.Phone,
                                   vlocity_cmt__ContactId__r.vlocity_cmt__SSN__c
                                   From vlocity_cmt__CustomerInteraction__c
                                  Where Id=:InteractionId];
                                System.debug('wwwww '+CustomerInteraction);
            if(CustomerInteraction.vlocity_cmt__AccountId__c!=NULL && CustomerInteraction.vlocity_cmt__ContactId__c==NULL){
            	businessFlag = true;
                //String statResourceURL = [select URL from StaticResource where Name = 'Task' LIMIT 1].URL;
                List<Account> relatedAccounts = New List<Account>();
                
                relatedAccounts = [Select Account_ID__c, Id,Name , Password__c, Customer_Class__c,Budget_Plan__c,vlocity_cmt__AutoPaymentCardType__c
                                   From Account
                                  Where ParentId =: CustomerInteraction.vlocity_cmt__AccountId__c];
                
                for(Account Acct: relatedAccounts){
                    accData.put(Acct.Id,Acct);
                } System.debug('111111 '+relatedAccounts);System.debug('22222 '+accData);
                if(!accData.isEmpty()){
                    SET<ID> keys = accData.keyset();
                    System.debug('33333' + (keys));
                	query = '';
					query += ' Select Service_Agreement__r.Account__c, Service_Agreement__r.Account_Name__c, '; 
			  		query += ' Service_Agreement__r.Account__r.Account_ID__c, Service_Agreement__r.Account__r.ParentId, ';
			  		query += ' Service_Agreement__r.Account__r.Parent.Name, Service_Agreement__r.Account__r.Parent.Account_ID__c, ';
			  		query += ' vlocity_cmt__PremisesId__r.Address__c, vlocity_cmt__PremisesId__r.Address_2__c, vlocity_cmt__PremisesId__r.Address_3__c, vlocity_cmt__PremisesId__r.Address_4__c, vlocity_cmt__PremisesId__r.vlocity_cmt__City__c, vlocity_cmt__PremisesId__r.County__c, vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c, vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c ';
			  		query += ' From vlocity_cmt__ServicePoint__c ';
                    query +=' Where Service_Agreement__r.Account__c IN :keys';
                    System.debug('444444 ' + (query));
				    spData = Database.query(query);
                    for(vlocity_cmt__ServicePoint__c sp:spData){
                        if(spDataMap.containsKey(sp.Service_Agreement__r.Account__c)){
                        	spDataMap.get(sp.Service_Agreement__r.Account__c).add(sp);	    
                        }
                        else{
                            spDataMap.put(sp.Service_Agreement__r.Account__c, New List<vlocity_cmt__ServicePoint__c>{sp});    
                        }
                    }
                }System.debug('zzzzz ' + spDataMap);
                gen.writeStartObject();
				gen.writeFieldName('ProfileInfo');
                gen.writeStartObject();
                	gen.writeObjectField('BusinessFlag', businessFlag);
                	gen.writeObjectField('PrimaryPhone', CustomerInteraction.vlocity_cmt__AccountId__r.Phone != null ? CustomerInteraction.vlocity_cmt__AccountId__r.Phone + ' ' : '');
               		gen.writeObjectField('SSN', '');
                	gen.writeObjectField('Email', CustomerInteraction.vlocity_cmt__AccountId__r.Email__c != null ? CustomerInteraction.vlocity_cmt__AccountId__r.Email__c + ' ' : '');
                	gen.writeObjectField('TIN', CustomerInteraction.vlocity_cmt__AccountId__r.vlocity_cmt__TaxID__c != null ? CustomerInteraction.vlocity_cmt__AccountId__r.vlocity_cmt__TaxID__c + ' ' : '');
                	gen.writeObjectField('Name', CustomerInteraction.vlocity_cmt__AccountId__r.Name != null ? CustomerInteraction.vlocity_cmt__AccountId__r.Name + ' ' : '');	

                gen.writeEndObject();
                
                gen.writeFieldName('RelatedAccounts');
                gen.writeStartArray();
                        for(vlocity_cmt__ServicePoint__c sp: spData){System.debug('ccccc ' + sp);
                        gen.writeStartObject();
                        String add1 = (sp.vlocity_cmt__PremisesId__r.Address__c != null ? sp.vlocity_cmt__PremisesId__r.Address__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.Address_2__c != null ? sp.vlocity_cmt__PremisesId__r.Address_2__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.Address_3__c != null ? sp.vlocity_cmt__PremisesId__r.Address_3__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.Address_4__c != null ? sp.vlocity_cmt__PremisesId__r.Address_4__c + ' ' : '');  			  		
                        String add2 = (sp.vlocity_cmt__PremisesId__r.vlocity_cmt__City__c != null ? sp.vlocity_cmt__PremisesId__r.vlocity_cmt__City__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.County__c != null ? sp.vlocity_cmt__PremisesId__r.County__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c != null ? sp.vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c != null ? sp.vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c + ' ' : '');			  		                    
                        gen.writeStringField('Premise', add1+add2);        	
                        gen.writeStringField('Account', sp.Service_Agreement__r.Account_Name__c);
                        gen.writeStringField('ACC', sp.Service_Agreement__r.Account__r.Account_ID__c);
                        gen.writeStringField('AccountId', sp.Service_Agreement__r.Account__c);
                        gen.writeBooleanField('defaultAccount', false);
                        if(accData.containsKey(sp.Service_Agreement__r.Account__c)){
                            gen.writeStringField('LastPayment', accData.get(sp.Service_Agreement__r.Account__c).Account_ID__c != null ? accData.get(sp.Service_Agreement__r.Account__c).Account_ID__c + ' ' : '');    
                            gen.writeStringField('Password', accData.get(sp.Service_Agreement__r.Account__c).Password__c != null ? accData.get(sp.Service_Agreement__r.Account__c).Password__c + ' ' : '');    
                            gen.writeStringField('Customerclass', accData.get(sp.Service_Agreement__r.Account__c).Customer_Class__c != null ? accData.get(sp.Service_Agreement__r.Account__c).Customer_Class__c + ' ' : '');    
                            gen.writeStringField('Budgetplan', accData.get(sp.Service_Agreement__r.Account__c).Budget_Plan__c != null ? accData.get(sp.Service_Agreement__r.Account__c).Budget_Plan__c + ' ' : '');    
    
                        }
                        else{
                            gen.writeStringField('LastPayment','');
                            gen.writeStringField('Password','');
                            gen.writeStringField('Customerclass','');
                            gen.writeStringField('Budgetplan','');
                        }
                        gen.writeEndObject();
                    }

               
                gen.writeEndArray();
                //end of the parent JSON object
                gen.writeEndObject();

            }
            else if(CustomerInteraction.vlocity_cmt__ContactId__c!=NULL){
                System.debug('666666 ');
                List<AccountContactRelation> accConRel= New List<AccountContactRelation>(); 
                accConRel = [Select ContactId, Account.Account_ID__c,Account.Name,AccountId, Account.Password__c, Account.Customer_Class__c, Account.Budget_Plan__c
                                                         From AccountContactRelation
                                                         Where ContactId=:CustomerInteraction.vlocity_cmt__ContactId__c];
               
                System.debug('77777 '+accConRel);
                for(AccountContactRelation Acct: accConRel){
                    accConRelData.put(Acct.AccountId,Acct);
                } System.debug('888888 '+accConRelData);
                if(!accConRelData.isEmpty()){
                     SET<ID> keys = accConRelData.keyset();
                	query = '';
					query += ' Select Service_Agreement__r.Account__c, Service_Agreement__r.Account_Name__c, '; 
			  		query += ' Service_Agreement__r.Account__r.Account_ID__c, Service_Agreement__r.Account__r.ParentId, ';
			  		query += ' Service_Agreement__r.Account__r.Parent.Name, Service_Agreement__r.Account__r.Parent.Account_ID__c, ';
			  		query += ' vlocity_cmt__PremisesId__r.Address__c, vlocity_cmt__PremisesId__r.Address_2__c, vlocity_cmt__PremisesId__r.Address_3__c, vlocity_cmt__PremisesId__r.Address_4__c, vlocity_cmt__PremisesId__r.vlocity_cmt__City__c, vlocity_cmt__PremisesId__r.County__c, vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c, vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c ';
			  		query += ' From vlocity_cmt__ServicePoint__c ';
                    query +='  Where Service_Agreement__r.Account__c IN :keys';
                    System.debug('99999 ' + (query));
				    spData = Database.query(query);
    
                }System.debug('aaaaaa ' + spData);
                gen.writeStartObject();
				gen.writeFieldName('ProfileInfo');
                gen.writeStartObject();
                	gen.writeObjectField('BusinessFlag', businessFlag);
                	//gen.writeObjectField('imageUrl', '/resource/'+statResourceURL);
                	gen.writeObjectField('PrimaryPhone', CustomerInteraction.vlocity_cmt__ContactId__r.Phone != null ? CustomerInteraction.vlocity_cmt__ContactId__r.Phone + ' ' : '');
               		gen.writeObjectField('SSN', CustomerInteraction.vlocity_cmt__ContactId__r.vlocity_cmt__SSN__c != null ? CustomerInteraction.vlocity_cmt__ContactId__r.vlocity_cmt__SSN__c + ' ' : '');
                	gen.writeObjectField('Email', CustomerInteraction.vlocity_cmt__ContactId__r.Email != null ? CustomerInteraction.vlocity_cmt__ContactId__r.Email + ' ' : '');
                	gen.writeObjectField('TIN', '');
                	gen.writeObjectField('Name', CustomerInteraction.vlocity_cmt__ContactId__r.Name != null ? CustomerInteraction.vlocity_cmt__ContactId__r.Name + ' ' : '');	
				gen.writeEndObject();
                
                gen.writeFieldName('RelatedAccounts');
                gen.writeStartArray();
                 for(vlocity_cmt__ServicePoint__c sp: spData){
                    gen.writeStartObject();
                    String add1 = (sp.vlocity_cmt__PremisesId__r.Address__c != null ? sp.vlocity_cmt__PremisesId__r.Address__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.Address_2__c != null ? sp.vlocity_cmt__PremisesId__r.Address_2__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.Address_3__c != null ? sp.vlocity_cmt__PremisesId__r.Address_3__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.Address_4__c != null ? sp.vlocity_cmt__PremisesId__r.Address_4__c + ' ' : '');  			  		
			  		String add2 = (sp.vlocity_cmt__PremisesId__r.vlocity_cmt__City__c != null ? sp.vlocity_cmt__PremisesId__r.vlocity_cmt__City__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.County__c != null ? sp.vlocity_cmt__PremisesId__r.County__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c != null ? sp.vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c + ' ' : '') + (sp.vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c != null ? sp.vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c + ' ' : '');			  		                    
                    gen.writeStringField('Premise', add1+add2);        	
                    gen.writeStringField('Account', sp.Service_Agreement__r.Account_Name__c);
                    gen.writeStringField('ACC', sp.Service_Agreement__r.Account__r.Account_ID__c);
                    gen.writeStringField('AccountId', sp.Service_Agreement__r.Account__c);
                    gen.writeBooleanField('defaultAccount', false);
                    if(accConRelData.containsKey(sp.Service_Agreement__r.Account__c)){
                    	gen.writeStringField('LastPayment', accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Account_ID__c != null ? accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Account_ID__c + ' ' : '');    
                        gen.writeStringField('Password', accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Password__c != null ? accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Password__c + ' ' : '');    
                    	gen.writeStringField('Customerclass', accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Customer_Class__c != null ? accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Customer_Class__c + ' ' : '');    
                    	gen.writeStringField('Budgetplan', accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Budget_Plan__c != null ? accConRelData.get(sp.Service_Agreement__r.Account__c).Account.Budget_Plan__c + ' ' : '');    

                    }
                    else{
                    	gen.writeStringField('LastPayment','');
                        gen.writeStringField('Password','');
                        gen.writeStringField('Customerclass','');
                        gen.writeStringField('Budgetplan','');
                    }
                    gen.writeEndObject();
                }
               
                gen.writeEndArray();
                //end of the parent JSON object
                gen.writeEndObject();

            }
            
        }
        returnJSONString = gen.getAsString();
        system.debug('!!!!!!!!!!!!!!!!!!!! '+returnJSONString);
        Map<String, Object> returnedJSON = (Map<String, Object>)JSON.deserializeUntyped(returnJSONString);
        //outMap = (Map<String, Object>)JSON.deserializeUntyped(returnJSONString);
		outMap.put('data', returnedJSON);
       
       
    }
}