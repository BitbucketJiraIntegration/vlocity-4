global with sharing class CC2020_IL_Person implements vlocity_cmt.VlocityOpenInterface2 {
    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
            List<String> contextIds = (List<String>) inputs.get('contextIds');    
            String parentId = (String) inputs.get('parentId');           
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');            
            String lastRecordId = request.lastRecordId;   // R15 pagination support
            
            //get values entered by the user            
            Map<String,Object> searchValueMap = request.searchValueMap;   
			System.debug('@@@ searchValueMap : ' + searchValueMap);         
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');            
            Integer pageSize = 0; // R15 pagination support            
            Integer numRecords = 30; // R15 pagination support            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');           
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('AccountNo');
            resultFieldList.add('Name');
            resultFieldList.add('Address1');
            resultFieldList.add('Address2');
            
            List<String> verificationFieldList = new List<String> ();
            
            List<String> optionalVerificationFieldList = new List<String> ();
            
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();            
            Map<String, String> newType1 = new Map<String, String> ();            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Account#');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountNo', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Address1', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Address2', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Name', newType1);

            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            //String uniqueRequestName = 'Contact';
            
            //need to set bundle name manually so commenting below line
            //String drBundleName = (String) resultInfo.get('drBundleName');
            
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            //String interactionObjName = 'Contact';
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap = new Map<String, Object>();
              }
               // R15 pagination support
            else{

                Set<String> AccountIdExt = New Set<String>();
                List<Object> data = null;
                list<Service_Agreement__c> conList = new List<Service_Agreement__c>();
                //Map<String,List<Case>> AccountIdMap = New Map<String, List<Case>>();                
                Boolean dynamicPersonLableAdded = false;
                Boolean dynamicBusinessLableAdded = false;
                List<List<Account>> searchList = New List<List<Account>>();
                if(searchValueMap != null && !searchValueMap.isEmpty()) {      
                	System.debug('into ifff');
        			data = this.getAccount(searchValueMap);                	
                }
                
                if(data != null) {
                    for(Object con : data) {
                        Map<String, object> resultValueMap = ((Map<String, object>)con);
                        String recordId = String.valueOf(resultValueMap.get('RecordId'));
                        String drBundleName;
                        if(resultValueMap.get('Object') != null) {
                        	drBundleName = String.valueOf(resultValueMap.get('Object')).equals('Account') ? 'CreateInteractionForAccount' : 'CreateInteractionForContact'; 
                        } else {
                    		continue;
                        } 
                        vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);                        
                        result.setOptionalVerificationFieldList(optionalVerificationFieldList);                   
                        results.add(result);
                    }
                }
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
                List<vlocity_cmt.LookupResult> resultSubSet =          
                new List<vlocity_cmt.LookupResult>();
                Integer startIndex = 0;
            
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                      {
                            if(results[i].recordId == lastRecordId)
                            {
                                  startIndex = i + 1;
                            }
                        }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }            
                output.put('lookupResults', resultSubset);                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
       	vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
                
        List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
        
        Map<String, Object> additionalData = request.additionalData;     
                
        List<String> searchFieldList = request.searchFieldList;
        searchFieldList.add('AddressCompound');
        searchFieldList.add('Name');
 		searchFieldList.add('BusinessName');
        searchFieldList.add('Phone');
 		searchFieldList.add('SSN');
 		searchFieldList.add('AlternateId');
 		searchFieldList.add('IncludeInactive');
         		
 		//adding option for the field
 		request.fieldPicklistValues.put('AddressCompound', new List<String> {'Premise','Mailing'});
 		       
        Map<String, Map<String, String>>labelTypeMap =   request.searchFieldsLabelTypeMap;
        Map<String, String> prop = new Map<String, String> ();
		prop.put('datatype','AddressCompound');
		prop.put('label1', 'Address');
		prop.put('value1', 'address');
		prop.put('value2', 'addressType');
		labelTypeMap.put('AddressCompound',prop);
 		
 		prop = new Map<String, String>();
 		prop.put('label','Individual Name');
        prop.put('datatype','text');         
        labelTypeMap.put('Name',prop);
		
		prop = new Map<String, String>();
        prop.put('label','Business Name');
        prop.put('datatype','text');         
        labelTypeMap.put('BusinessName',prop);         

        prop = new Map<String, String>();
        prop.put('label','Phone');
        prop.put('datatype','phone');
        labelTypeMap.put('Phone',prop);     
         
        prop = new Map<String, String>();
        prop.put('label','SSN');
        prop.put('datatype','text');
        labelTypeMap.put('SSN',prop); 
        
        prop = new Map<String, String>();
        prop.put('label','Alternate Id');
        prop.put('datatype','text');
        labelTypeMap.put('AlternateId',prop);       
   		
   		prop = new Map<String, String> ();
		prop.put('datatype','checkbox');
		prop.put('label', 'Include Inactive?');
		labelTypeMap.put('IncludeInactive',prop);
         
        Map<String, Object> valueMap = request.searchValueMap;
        output.put('lookupRequest', request);
    }
    
    global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);
        } else if (methodName == 'getSearchResults') {
            getSearchResults(inputs,output,options);
        }
       	Boolean success = true;      
       	return success;
    }
    
    public List<Object> getAccount(Map<String,Object> searchValueMap) {
    	List<Object> retData = new List<Object>();
    	
    	try {   	
			String query = '';
			Boolean includeInactive = !(searchValueMap.get('IncludeInactive') == null || !((Boolean)searchValueMap.get('IncludeInactive')));
			String address = null;
			Set<String> s = null;
			Map<Id, vlocity_cmt__ServicePoint__c> accData = new Map<Id, vlocity_cmt__ServicePoint__c>();
			if(searchValueMap.containsKey('addressType') && searchValueMap.get('addressType') != '' && 
			   searchValueMap.containsKey('address') && searchValueMap.get('address') != null ) {
			   	address = '%'+String.valueOf(searchValueMap.get('address'))+'%';
			   	s = new Set<String>();
			   	for(Object str : ((List<Object>)searchValueMap.get('addressType'))) {
			   		s.add(String.valueOf(str));
			   	}
		    }
		    
		    //get business accounts
		    Map<Id, Account> bussAcc = new Map<Id, Account>();
		    if(searchValueMap.containsKey('BusinessName') && searchValueMap.get('BusinessName') != null) {
		    	String bussName = '%' + String.valueOf(searchValueMap.get('BusinessName')) + '%';
		    	query += ' Select ParentId, Parent.Name, Parent.Account_ID__c ';
		    	query += ' FROM Account ';
		    	query += ' Where Parent.Name LIKE :bussName ';
		    	bussAcc = new Map<Id,Account>((List<Account>)Database.query(query));
		    } 
		    
		    //get contact based on name, phone, id and SSN
		    Map<Id, Contact> contactDtl = new Map<Id, Contact>();
		    if( (searchValueMap.containsKey('Name') && searchValueMap.get('Name') != null) ||
		    	(searchValueMap.containsKey('Phone') && searchValueMap.get('Phone') != null) ||
		    	(searchValueMap.containsKey('SSN') && searchValueMap.get('SSN') != null) ||
		    	(searchValueMap.containsKey('AlternateId') && searchValueMap.get('AlternateId') != null)) {
	    		
	    		String searchTerm = '';
	    		query = '';
	    		query += ' FIND ';
	    		if(searchValueMap.containsKey('Name') && searchValueMap.get('Name') != null) {
	    			searchTerm += '("' + String.valueOf(searchValueMap.get('Name')) + '")';
	    		}
	    		if(searchValueMap.containsKey('Phone') && searchValueMap.get('Phone') != null) {
	    			if(searchTerm != '') {
	    				searchTerm += ' OR ';
	    			}
	    			searchTerm += '("' + String.valueOf(searchValueMap.get('Phone')) + '")';
	    		}
	    		
	    		if(searchValueMap.containsKey('SSN') && searchValueMap.get('SSN') != null) {
	    			//if(searchTerm != '') {
	    			//	searchTerm += ' OR ';
	    			//}
	    			//if(String.valueOf(searchValueMap.get('SSN')).length() == 4) {
					//	searchTerm += '("*' + String.valueOf(searchValueMap.get('SSN')) + '" AND "'+ String.valueOf(searchValueMap.get('Name')) +'") ';
	    			//} else {
	    				searchTerm += '("' + String.valueOf(searchValueMap.get('SSN')) + '")';
	    			//}
	    		}
	    		if(searchValueMap.containsKey('AlternateId') && searchValueMap.get('AlternateId') != null) {
	    			if(searchTerm != '') {
	    				searchTerm += ' OR ';
	    			}
	    			searchTerm += '("' + String.valueOf(searchValueMap.get('AlternateId')) + '")';
	    		}
	    		query += ' :searchTerm IN ALL FIELDS RETURNING Contact(Id) ';
	    		System.debug('@@@ query  : ' + query);
	    		System.debug('@@@ searchTerm  : ' + searchTerm);
	    		contactDtl = new Map<Id,Contact>(((List<Contact>)Search.query(query)[0]));
	    		System.debug('@@ contactDtl : ' + contactDtl);
	    	}
	    	
	    	//to get actual acr details
		    List<AccountContactRelation> acrData;
		    Set<Id> accIdOfACR = new Set<Id>();
		    if(!contactDtl.isEmpty() || (s != null && s.contains('Mailing') && address != null)) {
		    	String whereClause = '';
		    	query = '';
			    query += ' Select AccountId, ContactId, Contact.Name '; 
			  	query += ' From AccountContactRelation ';
			  	if(!contactDtl.isEmpty()) {
			  		Set<Id> conId = contactDtl.keySet();
			  		whereClause += ' WHERE ContactId IN :conId ';
			  	}
			  	if(s != null && s.contains('Mailing') && address != null) {
			  		if(whereClause == '') {
			  			whereClause += ' WHERE Contact_Mailing_Address__c LIKE :address ';	
			  		} else {
			  			whereClause += ' AND Contact_Mailing_Address__c LIKE :address ';
			  		}
			  	}
			  	if(whereClause != '') {
			  		query += whereClause;
			  	}
			  	acrData = Database.query(query);
			  	for(AccountContactRelation a : acrData) {
			  		accIdOfACR.add(a.AccountId);	
			  	}
		    } 
		    
		    List<Contact> conWithSSN = new List<Contact>();
		    if(searchValueMap.containsKey('Name') && searchValueMap.get('Name') != null && 
		       searchValueMap.containsKey('SSN') && searchValueMap.get('SSN') != null && String.valueOf(searchValueMap.get('SSN')).length() == 4) {
		       	query = '';
		       	query += ' SELECT Id, Name, AccountId, Account.Account_ID__c FROM Contact ';
		       	query += ' Where Name like \'%'+String.escapeSingleQuotes(String.valueOf(searchValueMap.get('Name')))+'%\' ';
		       	query += ' AND vlocity_cmt__SSN__c like \'%'+String.escapeSingleQuotes(String.valueOf(searchValueMap.get('SSN')))+'\' ';
		       	if(!contactDtl.isEmpty()) {
			  		Set<Id> conId = contactDtl.keySet();
			  		query += ' AND ID NOT IN :conId ';
			  	}
		       	conWithSSN = Database.query(query);
		       	for(Contact c : conWithSSN) {
		       		accIdOfACR.add(c.AccountId);
		       	}
		       	
	        }
		       	
		    
		    
		    //to get final service point and premises
		    List<vlocity_cmt__ServicePoint__c> spData = null;
		    Map<Id, vlocity_cmt__ServicePoint__c> finalAccount = new Map<Id, vlocity_cmt__ServicePoint__c>();
		    if(!bussAcc.isEmpty() || !accIdOfACR.isEmpty() || (s != null && s.contains('Premise') && address != null)) {
		    	String whereClause = '';
		    	query = '';
				query += ' Select Service_Agreement__r.Account__c, Service_Agreement__r.Account_Name__c, '; 
			  	query += ' Service_Agreement__r.Account__r.Account_ID__c, Service_Agreement__r.Account__r.ParentId, ';
			  	query += ' Service_Agreement__r.Account__r.Parent.Name, Service_Agreement__r.Account__r.Parent.Account_ID__c, ';
			  	query += ' vlocity_cmt__PremisesId__r.Address__c, vlocity_cmt__PremisesId__r.Address_2__c, vlocity_cmt__PremisesId__r.Address_3__c, vlocity_cmt__PremisesId__r.Address_4__c, vlocity_cmt__PremisesId__r.vlocity_cmt__City__c, vlocity_cmt__PremisesId__r.County__c, vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c, vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c ';
			  	query += ' From vlocity_cmt__ServicePoint__c ';
			  	
			  	List<String> status = new List<String> {'Pending Start', 'Active', 'Pending Stop', 'Stopped', 'Reactivated'};
				if(!includeInactive) {
					if(whereClause != '')
						whereClause += ' AND Service_Agreement__r.SA_Status__c IN :status ';
					else
						whereClause += ' Service_Agreement__r.SA_Status__c IN :status ';
				}
				
				if(!bussAcc.isEmpty()) {
					Set<Id> ids = bussAcc.keySet();
					if(whereClause != '')
						whereClause += ' AND Service_Agreement__r.Account__c IN :ids ';
					else
						whereClause += ' Service_Agreement__r.Account__c IN :ids ';
				}
				
				
				Boolean bothAddress = false;
				if(s != null && s.contains('Premise') && address != null && !accIdOfACR.isEmpty()) {
					if( whereClause != '') {
						whereClause = ' ( ( ' + whereClause;
						bothAddress = true; 
					}	
				}
				
				if(s != null && s.contains('Premise') && address != null) {
					if(bothAddress) {
						whereClause += ' ) AND ( vlocity_cmt__PremisesId__r.Consolidated_Address__c LIKE :address ';
					} else {
						if(whereClause != '')
							whereClause += ' AND vlocity_cmt__PremisesId__r.Consolidated_Address__c LIKE :address ';
						else
							whereClause += ' vlocity_cmt__PremisesId__r.Consolidated_Address__c LIKE :address ';	
					}
					
				}
				
				if(!accIdOfACR.isEmpty()) {
					if(bothAddress) {
						whereClause += ' OR Service_Agreement__r.Account__c IN :accIdOfACR ';
					} else {
						if(whereClause != '') {
							whereClause += ' AND Service_Agreement__r.Account__c IN :accIdOfACR ';
						} else {
							whereClause += ' Service_Agreement__r.Account__c IN :accIdOfACR ';
						}
					}
				}
				
				if(whereClause != '') {
					whereClause = ' WHERE ' + whereClause;
				}
				if(bothAddress) {
					whereClause += ' ) ) ';
				}
				
				System.debug('premises query : ' + (query + whereClause));
				spData = Database.query(query + whereClause);
				for(vlocity_cmt__ServicePoint__c sp : spData) {
					if(!finalAccount.containsKey(sp.Service_Agreement__r.Account__c)) {
						finalAccount.put(sp.Service_Agreement__r.Account__c, sp);	
					}
				}
				
				Map<String,Object> record;
				
				for(Id aId : bussAcc.keySet()) {
					if(finalAccount.containsKey(aId)) {
						record = new Map<String,Object>();
						String add1 = (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_2__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_2__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_3__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_3__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_4__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_4__c + ' ' : '');  
			  			record.put('Address1', add1);
			  			String add2 = (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.County__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.County__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c + ' ' : '');
			  			record.put('Address2', add2);
						record.put('RecordId', aId);
			  			record.put('AccountNo', bussAcc.get(aId).Parent.Account_ID__c);
			  			record.put('Name', bussAcc.get(aId).Parent.Name);
			  			record.put('Object', 'Account');
			  			retData.add(record);
					}
				}
				
				Set<Id> excludedContact = new Set<Id>();
				if(acrData != null && acrData.size() > 0) {
					for(AccountContactRelation acrObj : acrData) {
						if(finalAccount.containsKey(acrObj.AccountId)) {
							Id aId = acrObj.AccountId;
							record = new Map<String,Object>();
							String add1 = (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_2__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_2__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_3__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_3__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_4__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_4__c + ' ' : '');  
				  			record.put('Address1', add1);
				  			String add2 = (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.County__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.County__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c + ' ' : '');
				  			record.put('Address2', add2);
							record.put('RecordId', acrObj.ContactId);
							record.put('AccountId', aId);
				  			record.put('AccountNo', finalAccount.get(aId).Service_Agreement__r.Account__r.Account_ID__c);
				  			record.put('Name', acrObj.Contact.Name);
				  			record.put('Object', 'Contact');
				  			retData.add(record);
				  			excludedContact.add(acrObj.ContactId);
						}
					}
				}
				
				if(conWithSSN != null && conWithSSN.size() > 0) {
					for(Contact con : conWithSSN) {
						if(finalAccount.containsKey(con.AccountId)) {
							Id aId = con.AccountId;
							record = new Map<String,Object>();
							String add1 = (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_2__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_2__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_3__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_3__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_4__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.Address_4__c + ' ' : '');  
				  			record.put('Address1', add1);
				  			String add2 = (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.County__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.County__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c + ' ' : '') + (finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c != null ? finalAccount.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c + ' ' : '');
				  			record.put('Address2', add2);
							record.put('RecordId', con.Id);
							record.put('AccountId', aId);
				  			record.put('AccountNo', finalAccount.get(aId).Service_Agreement__r.Account__r.Account_ID__c);
				  			record.put('Name', con.Name);
				  			record.put('Object', 'Contact');
				  			retData.add(record);
				  			excludedContact.add(con.Id);
						}
					}
				}
				
				//need to fetch contact which are satisfy individual name but dont have other details like Account, SA, SP & Premises
				if(includeInactive && searchValueMap.containsKey('Name') && searchValueMap.get('Name') != null && searchValueMap.get('Name') != '') {
					String name = '%'+String.valueOf(searchValueMap.get('Name'))+'%';
					query = '';
					query += ' Select Id, Name, AccountId From Contact Where Name LIKE :name ';
					if(!excludedContact.isEmpty()) {
						query += ' AND Id NOT IN :excludedContact ';
					}
					List<Contact> contactWithoutDetails = Database.query(query);
					for(Contact c : contactWithoutDetails) {
						record = new Map<String,Object>();
						record.put('RecordId', c.Id);
						record.put('Account1', '');
						record.put('Account2', '');
						record.put('AccountId', c.AccountId != null ? c.AccountId : '' );
			  			record.put('AccountNo', c.AccountId != null ? c.Account.Account_ID__c : '' );
			  			record.put('Name', c.Name);
			  			record.put('Object', 'Contact');
			  			retData.add(record);
					}
					
				}
		    }
		} catch(Exception e) {
			System.debug('@@ exception : ' + e.getStackTraceString());	
			System.debug('@@ exception : ' + e.getMessage());
		}
		System.debug('data to be return : ' + retData);
		return retData;
    }
    
    private list<AccountContactRelation> fetchAccountContForContact(Boolean singleContactAddressKeyword,Boolean onlyContactKeyword, Map<String,Object> searchValueMap){
    	List<AccountContactRelation> listToReturn = new List<AccountContactRelation>(); 
        Set<String> searchedSet = searchValueMap.keySet();
        String searchTypeSOSL = '';
        String searchTerms = '';
        String whereClauseForContact = '';
        Set<Id>ContactIds = new Set<Id>();
        String AccountContactRelationQuery = '';
        List<Contact> contactList = new List<Contact>();
        if(singleContactAddressKeyword || onlyContactKeyword){
            String searchedField = new List<String>(searchedSet)[0];
            searchTypeSOSL =  'FIND ';
            if(searchedField=='Name'){
                searchTypeSOSL+='\''+searchValueMap.get(searchedField)+'\' IN NAME FIELDS RETURNING Contact (Id)';		    
            }
            else if(searchedField=='Phone'){
                searchTypeSOSL += '\''+searchValueMap.get(searchedField)+'\' IN PHONE FIELDS RETURNING Contact (Id)';                    	    
            }
            else{
                searchTypeSOSL+= '\''+searchValueMap.get(searchedField)+'\' IN ALL FIELDS RETURNING Contact (Id)';                    	                            
            }
            
        }
        else if ((!singleContactAddressKeyword) && (!onlyContactKeyword)){
            
            if(searchedSet.contains('Address')){
                searchedSet.remove('Address');    
            }
            
            for(String st:searchedSet){
                if(!String.isEmpty(searchTerms)){
                    searchTerms = '("' + st;	    
                }
                else{
                    searchTerms += ' OR '+ st;    
                }
                searchTerms +='")';
                
                
                
                if(String.isNotEmpty(whereClauseForContact)){
                    whereClauseForContact+= ' AND ';    
                }
                if(st=='Name'){
                    whereClauseForContact+= ' Name ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                else if(st=='BusinessName'){
                    //whereClauseForContact+= ' Name ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                else if(st=='Phone'){
                    whereClauseForContact+= ' Phone ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                else if(st=='SSN'){
                    whereClauseForContact+= ' vlocity_cmt__SSN__c ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                searchTypeSOSL = 'FIND '+searchTerms+'\' IN ALL FIELDS RETURNING Contact (Id)'+whereClauseForContact;
            }	    
        }
        if(!String.isEmpty(searchTypeSOSL)){
            List<List<sObject>> resultContacts = search.query(searchTypeSOSL);
            contactList = resultContacts[0];     
        }
                
        for(Contact ct:contactList){
            ContactIds.add(ct.Id);    
        }
        Map<String,AccountContactRelation> accConRelmap = new Map<String,AccountContactRelation>();
        String accountIdsExt = '';
        if(!ContactIds.isEmpty() && searchValueMap.keySet().contains('Address')){
            AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c,  Account.Customer_Class__c, Account.Customer_Class_Code__c FROM AccountContactRelation WHERE ';                        
            AccountContactRelationQuery+=  ' ContactId IN '+ ContactIds ;
            system.debug('AccountContactRelationQuery===='+AccountContactRelationQuery);
            listToReturn = Database.query(AccountContactRelationQuery);
            
        }
        return listToReturn;
    }
}