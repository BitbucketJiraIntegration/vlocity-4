public with sharing class PGE_InteractionTrackingController {
    public String layoutName {get;set;}
    public String nsp { get{return 'vlocity_cmt__';} set;}
    //public String nsp = 'vlocity_cmt__';
     
    public PGE_InteractionTrackingController(){       
    }
     
    @RemoteAction
    public static Map<String, Object> trackChanges(List<Map<String, Object>> trackingData)
    {
        return vlocity_cmt.VlocityTrackingService.track(trackingData);
    }      

}