global with sharing class CCB_ILContactsOrPremises implements vlocity_cmt.VlocityOpenInterface2
{  
    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
        {
            System.debug('ino getSearchResults inputs : ' + inputs);
            System.debug('ino getSearchResults output : ' + output);
            System.debug('ino getSearchResults options : ' + options);            
            List<String> contextIds = (List<String>) inputs.get('contextIds');
            String parentId = (String) inputs.get('parentId');            
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');           
            String lastRecordId = request.lastRecordId;   // R15 pagination support
            
            //get values entered by the user            
            Map<String,Object> searchValueMap = request.searchValueMap;           
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');           
            Integer pageSize = 0; // R15 pagination support           
            Integer numRecords = 30; // R15 pagination support            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');
            
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;
            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('PersonName');
            resultFieldList.add('AccountId');
            resultFieldList.add('PersonId');
            
            List<String> verificationFieldList = new List<String> ();
            verificationFieldList.add('PersonName'); 
            
            List<String> optionalVerificationFieldList = new List<String> ();
            optionalVerificationFieldList.add('AccountId');
            optionalVerificationFieldList.add('PersonId');
            
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();
            
            Map<String, String> newType1 = new Map<String, String> ();            
            newType1.put('label','Account Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Person Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Name');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonName', newType1);
            
           
            
            //modified
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Phone');
            newType1.put('datatype','Phone');            
            resultFieldsLabelTypeMap.put('Phone', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Employment Status');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__EmploymentStatus__c', newType1); 
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'MailingAddress');
            newType1.put('datatype','Address');            
            resultFieldsLabelTypeMap.put('Mailing_Address__c', newType1);  
  
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Driver License');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);           

            newType1 = new Map<String, String> ();
            newType1.put('label', 'Primises');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Primises_Address__c', newType1);
            
            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            String drBundleName = (String) resultInfo.get('drBundleName');
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap =
                new Map<String, Object>();
            
                resultValueMap.put('RelatedParty', 'John Smith');
                resultValueMap.put('Role', 'Carrier');
                resultValueMap.put('PartyName', 'John Smith');
                resultValueMap.put('Address', '50 Fremont, San Francisco, CA');
                
                String recordId = 'ExternalId';
            
                vlocity_cmt.LookupResult result =  new vlocity_cmt.LookupResult (resultFieldsLabelTypeMap,resultFieldList,resultValueMap, recordId, uniqueRequestName, request, verificationResult,verificationFieldList,drBundleName,interactionObjName);              
                result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                
                // R15 optional verification fields support
            
                results.add(result);
              }
               // R15 pagination support
            else{
                list<Contact> conList = new List<Contact>();
                if(searchValueMap != null && !searchValueMap.isEmpty()) {

                    String queryString='';                    
                    String whereClause = '';               
                    System.debug('@@searchValueMap : ' + searchValueMap);
                    
                    String valueSearch = '';
                    
                    for(String field : searchValueMap.keySet()) {
                        if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                            
                            if(valueSearch == '') {
                                valueSearch += ' {"' + searchValueMap.get(field) + '" ';
                            } else {
                                valueSearch += ' AND "' + searchValueMap.get(field) + '"';
                            }
                          
                        }
                    }                        
                    if(valueSearch != '') {
                        valueSearch += '} ';
                        queryString = 'FIND ' + valueSearch + ' IN ALL FIELDS RETURNING Contact(Id, Name, FirstName, LastName, Person_ID__c, Account.Account_ID__c, Phone,LeadSource,Birthdate,Mailing_Address__c, Primises_Address__c, Email, vlocity_cmt__EmploymentStatus__c, vlocity_cmt__Occupation__c,vlocity_cmt__NetWorth__c,vlocity_cmt__SSN__c,vlocity_cmt__DriversLicenseNumber__c,Passport__c ';
                        if(parentId != null && parentId != '') {
                            queryString += ' where AccountId = :parentId ';
                        }
                        queryString += ' ) ';
                        System.debug('final query : ' + queryString);
                        List<List<SObject>> contacts = search.query(queryString );
                        conList  = ((List<contact>)contacts[0]);
                        System.debug('@@ contacts : ' + contacts);
                    }
                }
                for(Contact con : conList)                    
                {
                    Map<String, object> resultValueMap = new Map<String, object>();
                    resultValueMap.put('PersonName',con.Name != null ? con.Name : '');
                    resultValueMap.put('PersonId',con.Person_ID__c != null ? con.Person_ID__c : '');
                    resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');
                    String recordId = String.valueOf(con.get('Id'));
                    vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);                             
                    result.setOptionalVerificationFieldList(optionalVerificationFieldList);               
                    results.add(result);
                }
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
              List<vlocity_cmt.LookupResult> resultSubSet =          
              new List<vlocity_cmt.LookupResult>();
              Integer startIndex = 0;
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                    {
                        if(results[i].recordId == lastRecordId)
                        {
                            startIndex = i + 1;
                        }
                    }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }
            
                output.put('lookupResults', resultSubset);
                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
     {
            
               vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
                
                List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
                
                Map<String, Object> additionalData = request.additionalData;     
                
                List<String> searchFieldList = request.searchFieldList;
                searchFieldList.add('Address');
         		searchFieldList.add('CheckBox');
                searchFieldList.add('Name');
         		searchFieldList.add('BusinessName');
                searchFieldList.add('Phone');
         		searchFieldList.add('SSN');
         		searchFieldList.add('AlternateId');
                       
                Map<String, Map<String, String>>labelTypeMap =   request.searchFieldsLabelTypeMap;
                
         		//addressType
                Map<String, String> addressType = new Map<String, String> ();
                addressType.put('label','Address');
                addressType.put('datatype','textarea');
                labelTypeMap.put('Address',addressType);

        		//MailingCheckboxType
                Map<String, String> CheckboxType = new Map<String, String> ();
                CheckboxType.put('checkbox1label','Premise');
         		CheckboxType.put('checkbox1Id','checkBoxId');
         		CheckboxType.put('checkbox2label','Mailing');
         		CheckboxType.put('checkbox2Id','checkBoxId');
                CheckboxType.put('datatype','CheckBox');
         
                labelTypeMap.put('CheckBox',CheckboxType);
             	//newType2
                Map<String, String> newType2 = new Map<String, String> ();
                newType2.put('label','Individual Name');
                newType2.put('datatype','text');         
                labelTypeMap.put('Name',newType2);
				
				//newType2
                Map<String, String> BusinessNameType = new Map<String, String> ();
                BusinessNameType.put('label','Business Name');
                BusinessNameType.put('datatype','text');         
                labelTypeMap.put('BusinessName',BusinessNameType);         

                // newType3
                Map<String, String> newType3 = new Map<String, String> ();
                newType3.put('label','Phone');
                newType3.put('datatype','phone');
                labelTypeMap.put('Phone',newType3);     
                 
                // newType4
                Map<String, String> newType4 = new Map<String, String> ();
                newType4.put('label','SSN');
                newType4.put('datatype','text');
                labelTypeMap.put('SSN',newType4); 
                
                Map<String, String> newType5 = new Map<String, String> ();
                newType5.put('label','Alternate Id');
                newType5.put('datatype','text');
                labelTypeMap.put('AlternateId',newType5); 
                
        
           
                Map<String, Object> valueMap = request.searchValueMap;
                
                //valueMap.put('FirstName',null);
              //  valueMap.put('LastName',null);
                
                System.debug(' LookupRequest is '+request); 
                output.put('lookupRequest', request);
       
    }
global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
{
  
        
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);
         
         
        //  throw new AuraHandledException('ccccccccccccccccc');       
         
         } 
        
    else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
        
   // getSearchResults(methodName,output,options);
   Boolean success = true;
  
return success;
}
}