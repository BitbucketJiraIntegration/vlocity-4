global class viewPersonsList implements vlocity_cmt.VlocityOpenInterface{

    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean result = true;
        System.debug('In Method');
        try
        {
            if (methodName.equals('GetPersonsList'))
            {
                GetPersonsList(inputMap,outMap);
            }
            else 
            {
                result = false;
            }   
        } 
        catch(Exception e)
        {
            System.debug('Person List:invokeMethod -> exception: '+e);
            result = false;
        }
        
        return result;
    }
    
void GetPersonsList(Map<String, Object> inputMap, Map<String, Object> outMap)
   {
        String sAccountId = (String)inputMap.get('Id');
		System.debug('sAccountId-->'+sAccountId);
        List<Map<String, String>>  resultList = new List<Map<String,String>>();
       	List<AccountContactRelation> acrList = [select Id, FIN_RESP_SW__c, MAIN_CUST_SW__c, 
                                                Contact.vlocity_cmt__SocialSecurityNumber__c, Contact.Name, Contact.Id,
                                                AccountId
                                                from AccountContactRelation where AccountId =: sAccountId];
		
       
        if(acrList.size() > 0) {
            String MainCustomer = '';
            String FinanaciallyResponsible = '';
            System.debug('acrList--->'+acrList);
            System.debug('acrList--->'+acrList.size());
            
            for (AccountContactRelation acr : acrList) {
                Map<String, String> acrMap = new Map<String, String>();
                
                MainCustomer = (acr.MAIN_CUST_SW__c == true? 'Yes' : 'No');
                FinanaciallyResponsible = (acr.FIN_RESP_SW__c == true? 'Yes' : 'No');
                acrMap.put('Edit', '');
                acrMap.put('Delete', '');	
                acrMap.put('AccConRelId', acr.Id);
                acrMap.put('ContactId', acr.Contact.Id);
                acrMap.put('ContextId', acr.AccountId);
                acrMap.put('Finanacially Responsible', FinanaciallyResponsible);
                acrMap.put('Main Customer', MainCustomer);
				acrMap.put('Social Security Number', acr.Contact.vlocity_cmt__SocialSecurityNumber__c);
                acrMap.put('Name', acr.Contact.Name);
                
                resultList.add(acrMap);
            }
            System.debug('resultList--->'+resultList);
            System.debug('acrList--->'+acrList);
            //outMap.put('payment-creation-step1', new List<Object>{ new Map<String, Object> { 'acrList' => resultList} } );
            outMap.put('ViewPersons', new Map<String, Object> {'OmniScript' => resultList});
            System.debug('outMap--->'+outMap);
        } else{
           outMap.put('acrList', 'NOT FOUND');
        }
    }        
}