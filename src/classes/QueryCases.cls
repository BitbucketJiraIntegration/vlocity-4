global with sharing class QueryCases implements vlocity_cmt.VlocityOpenInterface
{
    global QueryCases() {}

    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean result = true;
        try
        {
            if (methodName.equals('GetCaseData'))
            {
                GetCaseData(inputMap,outMap);
            }
            else 
            {
                result = false;
            }   
        } 
        catch(Exception e)
        {
            System.debug('QueryCases:invokeMethod -> exception: '+e);
            result = false;
        }
        
        return result;
    }
    
void GetCaseData(Map<String, Object> inputMap, Map<String, Object> outMap)
   {
        String sAccountId = (String)inputMap.get('AccountId');
        System.debug('Id Entered: ' + sAccountId);
    
        List<Case> caseList = [SELECT AccountId, Description, Priority, Type, Subject, Status, Origin, CreatedDate FROM Case WHERE AccountId=:sAccountId];
     
        if(caseList != null && caseList.size() > 0)
        {
           List<Map<String, String>>  returnList = new List<Map<String,String>>();
           for (Case c : caseList)
           {           
               Map<String, String> tempMap = new Map<String, String>();
               tempMap.put('CaseAccountId', c.AccountId);
               tempMap.put('CaseId', c.Id);
               tempMap.put('CaseDescription', c.Description);
               tempMap.put('CaseSubject', c.Subject);
               tempMap.put('CaseOrigin', c.Origin);
               tempMap.put('CasePriority', c.Priority);
               tempMap.put('CaseStatus', c.Status);
               tempMap.put('CaseType', c.Type);
               tempMap.put('CaseCreatedDate', c.CreatedDate.format('MM/dd/yyyy'));                             
                                                         
               returnList.add(tempMap);
           }
            
           outMap.put('options', returnList);
           System.debug('These are my Cases' + returnList);
           
           
        }
        else{
           outMap.put('caseList', 'NOT FOUND');
        }
    }        
}