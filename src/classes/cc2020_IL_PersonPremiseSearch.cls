global with sharing class cc2020_IL_PersonPremiseSearch implements vlocity_cmt.VlocityOpenInterface2
{  
    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
        {
            System.debug('ino getSearchResults inputs : ' + inputs);
            System.debug('ino getSearchResults output : ' + output);
            System.debug('ino getSearchResults options : ' + options);            
            List<String> contextIds = (List<String>) inputs.get('contextIds');
            String parentId = (String) inputs.get('parentId');            
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');           
            String lastRecordId = request.lastRecordId;   // R15 pagination support
            
            //get values entered by the user            
            Map<String,Object> searchValueMap = request.searchValueMap;           
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');           
            Integer pageSize = 0; // R15 pagination support           
            Integer numRecords = 30; // R15 pagination support            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');
            
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;
            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('AccountId');
            resultFieldList.add('PersonName');      
            resultFieldList.add('DoingBusinessAs');
            resultFieldList.add('StreetAddress');
            resultFieldList.add('City');
            resultFieldList.add('State');
            resultFieldList.add('Zipcode');
            
            List<String> verificationFieldList = new List<String> ();
            verificationFieldList.add('PersonName'); 
            
            List<String> optionalVerificationFieldList = new List<String> ();
            optionalVerificationFieldList.add('AccountId');
            //optionalVerificationFieldList.add('PersonId');
            
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();
            
            Map<String, String> newType1 = new Map<String, String> ();            
            newType1.put('label','Account Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Name/Business Name');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonName', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Doing Business As');
            newType1.put('datatype','text');      
            resultFieldsLabelTypeMap.put('DoingBusinessAs', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Street Address');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('StreetAddress', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','City');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('City', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','State');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('State', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Zipcode');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Zipcode', newType1);           
            
            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            String drBundleName = (String) resultInfo.get('drBundleName');
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
               
            }
            // R15 pagination support
            else{
				String premiseObjectClause = '';
                Set<Id>ContactIds = new Set<Id>();//
                //List<vlocity_cmt__Premises__c> premiseServicePointList = new List<vlocity_cmt__Premises__c>();
                Map<String,List<vlocity_cmt__Premises__c>> AccountPremiseMap = new Map<String,List<vlocity_cmt__Premises__c>>();
                List<Contact> contactList = new List<Contact>();//
                String AccountExternalIds = '';
                String AccountContactRelationQuery = '';//////
                List<AccountContactRelation> initialData = null;
                List<AccountContactRelation> data = null;
                String whereClauseForContact = '';///
                String searchTypeSOSL = '';//////////////
                Boolean singleContactAddressKeyword= false;
                Boolean onlyContactKeyword= false;
                Set<String> searchedSet = searchValueMap.keySet();/////
                String CompleteSOSL = '';
                String searchTerms = '';///////
                if(searchValueMap.keySet().contains('Address') && searchValueMap.size()==2){
                	singleContactAddressKeyword	= true;
                    searchedSet.remove('Address');
                    
                }
                else if(!searchValueMap.keySet().contains('Address') && searchValueMap.size()==1){
                	onlyContactKeyword = true;	    
                }
                //for below code sample resuable method is in progress it is written in the last of this class and for few variables ///// is appended, those vaariables not not to use in this because those are initialized in reusabel method.
                if(singleContactAddressKeyword || onlyContactKeyword){
                    String searchedField = new List<String>(searchedSet)[0];
                    searchTypeSOSL =  'FIND ';
                    if(searchedField=='Name'){
                    	searchTypeSOSL+='\''+searchValueMap.get(searchedField)+'\' IN NAME FIELDS RETURNING Contact (Id)';		    
                    }
                    else if(searchedField=='Phone'){
                    	searchTypeSOSL += '\''+searchValueMap.get(searchedField)+'\' IN PHONE FIELDS RETURNING Contact (Id)';                    	    
                    }
                    else{
                    	searchTypeSOSL+= '\''+searchValueMap.get(searchedField)+'\' IN ALL FIELDS RETURNING Contact (Id)';                    	                            
                    }
                	  
                }
                else if ((!singleContactAddressKeyword) && (!onlyContactKeyword)){
                    
                    if(searchedSet.contains('Address')){
                    	searchedSet.remove('Address');    
                    }
                    
                    for(String st:searchedSet){
                        if(!String.isEmpty(searchTerms)){
                        	searchTerms = '("' + st;	    
                        }
                        else{
                        	searchTerms += ' OR '+ st;    
                        }
                        searchTerms +='")';
                        
                        
                       
						if(String.isNotEmpty(whereClauseForContact)){
                        	whereClauseForContact+= ' AND ';    
                        }
                        if(st=='Name'){
                        	whereClauseForContact+= ' Name ='+'\''+searchValueMap.get(st)+'\'';		    
                        }
                        else if(st=='BusinessName'){
                        	//whereClauseForContact+= ' Name ='+'\''+searchValueMap.get(st)+'\'';		    
                        }
                        else if(st=='Phone'){
                        	whereClauseForContact+= ' Phone ='+'\''+searchValueMap.get(st)+'\'';		    
                        }
                        else if(st=='SSN'){
                        	whereClauseForContact+= ' vlocity_cmt__SSN__c ='+'\''+searchValueMap.get(st)+'\'';		    
                        }
                         searchTypeSOSL = 'FIND '+searchTerms+'\' IN ALL FIELDS RETURNING Contact (Id)'+whereClauseForContact;
                    }	    
                }
                if(!String.isEmpty(searchTypeSOSL)){
                	List<List<sObject>> resultContacts = search.query(searchTypeSOSL);
    				contactList = resultContacts[0];     
                }
                
                for(Contact ct:contactList){
                	ContactIds.add(ct.Id);    
                }
                Map<String,AccountContactRelation> accConRelmap = new Map<String,AccountContactRelation>();
                String accountIdsExt = '';
                if(!ContactIds.isEmpty() && searchValueMap.keySet().contains('Address')){
                	AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c,  Account.Customer_Class__c, Account.Customer_Class_Code__c FROM AccountContactRelation WHERE ';                        
                        AccountContactRelationQuery+=  ' ContactId IN '+ ContactIds ;
                    	system.debug('AccountContactRelationQuery===='+AccountContactRelationQuery);
                        initialData = Database.query(AccountContactRelationQuery);
                        
                    for(AccountContactRelation acrr:initialData){
							if(String.isNotEmpty(accountIdsExt)){
                                    accountIdsExt = accountIdsExt+'\''+acrr.Account.Account_ID__c+'\',';    
                                 }
                                 else{
                                    accountIdsExt = '('+'\''+acrr.Account.Account_ID__c+'\',';    
                                 }
                        	accConRelmap.put(String.valueOf(acrr.Account.Account_ID__c)+','+String.valueOf(acrr.Contact.Person_ID__c),acrr);		     
                        }
                    	accountIdsExt = accountIdsExt.removeEnd(',');
                        accountIdsExt = accountIdsExt+')';
                    	premiseObjectClause = 'Select Consolidated_Address__c, Id , Address__c , Address_2__c ,Address_3__c ,Address_4__c , vlocity_cmt__City__c, County__c , vlocity_cmt__Country__c, vlocity_cmt__PostalCode__c,Consolidated_Street__c, vlocity_cmt__State__c, (Select Id, vlocity_cmt__ServicePointNumber__c , Service_Agreement__r.Account__r.Account_ID__c From vlocity_cmt__ServicePoints__r where Service_Agreement__r.Account__r.Account_ID__c IN '+accountIdsExt+') From vlocity_cmt__Premises__c Where Consolidated_Address__c ';
                        premiseObjectClause+=' LIKE  \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get('Address'))) +'%\'';
                        
                    	for(vlocity_cmt__Premises__c Premises:Database.query(premiseObjectClause)){
                        system.debug('@@@@@@'+Premises);
                        for(vlocity_cmt__ServicePoint__c sp : Premises.vlocity_cmt__ServicePoints__r){
                            
                            if(String.isNotBlank(sp.Service_Agreement__r.Account__r.Account_ID__c)){
                                system.debug('######'+sp.Service_Agreement__r.Account__r.Account_ID__c);
                            	if(String.isNotEmpty(AccountExternalIds)){
                                    AccountExternalIds = AccountExternalIds+'\''+sp.Service_Agreement__r.Account__r.Account_ID__c+'\',';    
                                 }
                                 else{
                                    AccountExternalIds = '('+'\''+sp.Service_Agreement__r.Account__r.Account_ID__c+'\',';    
                                 }
                                if(AccountPremiseMap.containsKey(sp.Service_Agreement__r.Account__r.Account_ID__c)){
                                	AccountPremiseMap.get(sp.Service_Agreement__r.Account__r.Account_ID__c).add(Premises);	    
                                }
                                else{
                                    AccountPremiseMap.put(sp.Service_Agreement__r.Account__r.Account_ID__c,new List<vlocity_cmt__Premises__c>{Premises});    
                                }
                            }
                           
                       }
			    	   
                    }
                    String splittedAccountId;
                    for(String str:accConRelmap.keySet()){
                    	splittedAccountId = str.substringBefore(',');
                        for(vlocity_cmt__Premises__c ac:AccountPremiseMap.get(splittedAccountId)){
                        	Map<String, object> resultValueMap = new Map<String, object>();
                            resultValueMap.put('PersonName',accConRelmap.get(str).Contact.Name != null ? accConRelmap.get(str).Contact.Name : '');
                            resultValueMap.put('AccountId',splittedAccountId != null ? splittedAccountId : '');
                            resultValueMap.put('DoingBusinessAs',accConRelmap.get(str).Account.Customer_Class_Code__c != null ? accConRelmap.get(str).Account.Customer_Class_Code__c : '');
                            resultValueMap.put('StreetAddress',ac.Consolidated_Street__c != null ? ac.Consolidated_Street__c : '');
                            resultValueMap.put('City',ac.vlocity_cmt__City__c != null ? ac.vlocity_cmt__City__c : '');
                            resultValueMap.put('State',ac.vlocity_cmt__State__c != null ? ac.vlocity_cmt__State__c : '');
                            resultValueMap.put('Zipcode',ac.vlocity_cmt__PostalCode__c != null ? ac.vlocity_cmt__PostalCode__c : '');
                            String recordId = accConRelmap.get(str).Contact.Id;  
                        
                            vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);         
                            
                            result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                       
                            results.add(result); 	   
                        }
                    }
                }                
                else if(ContactIds.isEmpty() && searchValueMap.keySet().contains('Address')){
                	    String searchedAddress = '%'+String.escapeSingleQuotes(String.valueOf(searchValueMap.get('Address')))+'%';
                    	premiseObjectClause = 'Select Consolidated_Address__c, Id , Address__c , Address_2__c ,Address_3__c ,Address_4__c , vlocity_cmt__City__c, County__c , vlocity_cmt__Country__c, vlocity_cmt__PostalCode__c,Consolidated_Street__c, vlocity_cmt__State__c, (Select Id, vlocity_cmt__ServicePointNumber__c , Service_Agreement__r.Account__r.Account_ID__c From vlocity_cmt__ServicePoints__r where Service_Agreement__r.Account__r.Account_ID__c ) From vlocity_cmt__Premises__c Where Consolidated_Address__c ';
                        premiseObjectClause+=' LIKE  \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get('Address'))) +'%\'';    					
                        for(vlocity_cmt__Premises__c Premises:Database.query(premiseObjectClause)){                           
                            for(vlocity_cmt__ServicePoint__c sp : Premises.vlocity_cmt__ServicePoints__r){
                                if(searchValueMap.keySet().contains('Address') && searchValueMap.size()>1){
                                    
                                }
                                if(String.isNotBlank(sp.Service_Agreement__r.Account__r.Account_ID__c)){
                                    system.debug('######'+sp.Service_Agreement__r.Account__r.Account_ID__c);
                                    if(String.isNotEmpty(AccountExternalIds)){
                                        AccountExternalIds = AccountExternalIds+'\''+sp.Service_Agreement__r.Account__r.Account_ID__c+'\',';    
                                     }
                                     else{
                                        AccountExternalIds = '('+'\''+sp.Service_Agreement__r.Account__r.Account_ID__c+'\',';    
                                     }
                                    if(AccountPremiseMap.containsKey(sp.Service_Agreement__r.Account__r.Account_ID__c)){
                                        AccountPremiseMap.get(sp.Service_Agreement__r.Account__r.Account_ID__c).add(Premises);	    
                                    }
                                    else{
                                        AccountPremiseMap.put(sp.Service_Agreement__r.Account__r.Account_ID__c,new List<vlocity_cmt__Premises__c>{Premises});    
                                    }
                                }
                               
                           }
                           
                        }
                    
                     if(String.isNotBlank(AccountExternalIds)){
                        AccountExternalIds = AccountExternalIds.removeEnd(',');
                        AccountExternalIds = AccountExternalIds+')';
                        AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c,  Account.Customer_Class__c, Account.Customer_Class_Code__c FROM AccountContactRelation WHERE ';                        
                        AccountContactRelationQuery+=  '  MAIN_CUST_SW__c = true AND ';      
                        AccountContactRelationQuery+=  ' Account.Account_ID__c IN '+ AccountExternalIds ;
                    	system.debug('AccountContactRelationQuery===='+AccountContactRelationQuery);
                        data = Database.query(AccountContactRelationQuery);
                        system.debug('data===='+data);
                    }
                    
                    if(data != null) {       
                    for(AccountContactRelation con : data) {                  
                        System.debug('Sobjet==='+ con);
                        for(vlocity_cmt__Premises__c Premise:AccountPremiseMap.get(con.Account.Account_ID__c)){
                        	Map<String, object> resultValueMap = new Map<String, object>();
                            resultValueMap.put('PersonName',con.Contact.Name != null ? con.Contact.Name : '');
                            resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');
                            resultValueMap.put('DoingBusinessAs',con.Account.Customer_Class_Code__c != null ? con.Account.Customer_Class_Code__c : '');
                            resultValueMap.put('StreetAddress',Premise.Consolidated_Street__c != null ? Premise.Consolidated_Street__c : '');
                            resultValueMap.put('City',Premise.vlocity_cmt__City__c != null ? Premise.vlocity_cmt__City__c : '');
                            resultValueMap.put('State',Premise.vlocity_cmt__State__c != null ? Premise.vlocity_cmt__State__c : '');
                            resultValueMap.put('Zipcode',Premise.vlocity_cmt__PostalCode__c != null ? Premise.vlocity_cmt__PostalCode__c : '');
                            String recordId = con.Contact.Id;  
                        
                            vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);         
                            
                            result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                       
                            results.add(result);	    
                        } 
                    }
                }
                }
                else if(!ContactIds.isEmpty() && !searchValueMap.keySet().contains('Address')){
                    
                }
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
              List<vlocity_cmt.LookupResult> resultSubSet =          
              new List<vlocity_cmt.LookupResult>();
              Integer startIndex = 0;
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                    {
                        if(results[i].recordId == lastRecordId)
                        {
                            startIndex = i + 1;
                        }
                    }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }
            
                output.put('lookupResults', resultSubset);
                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
     {            
               vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
                
                List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
                
                Map<String, Object> additionalData = request.additionalData;     
                
                List<String> searchFieldList = request.searchFieldList;
                searchFieldList.add('Address');
         		searchFieldList.add('CheckBox');
                searchFieldList.add('Name');
         		searchFieldList.add('BusinessName');
                searchFieldList.add('Phone');
         		searchFieldList.add('SSN');
         		searchFieldList.add('AlternateId');
                       
                Map<String, Map<String, String>>labelTypeMap =   request.searchFieldsLabelTypeMap;
                
         		//addressType
                Map<String, String> addressType = new Map<String, String> ();
                addressType.put('label','Address');
                addressType.put('datatype','textarea');
                labelTypeMap.put('Address',addressType);

        		//MailingCheckboxType
                Map<String, String> CheckboxType = new Map<String, String> ();
                CheckboxType.put('checkbox1label','Premise');
         		CheckboxType.put('checkbox1Id','checkBoxId');
         		CheckboxType.put('checkbox2label','Mailing');
         		CheckboxType.put('checkbox2Id','checkBoxId');
                CheckboxType.put('datatype','CheckBox');
         
                labelTypeMap.put('CheckBox',CheckboxType);
             	//newType2
                Map<String, String> newType2 = new Map<String, String> ();
                newType2.put('label','Individual Name');
                newType2.put('datatype','text');         
                labelTypeMap.put('Name',newType2);
				
				//newType2
                Map<String, String> BusinessNameType = new Map<String, String> ();
                BusinessNameType.put('label','Business Name');
                BusinessNameType.put('datatype','text');         
                labelTypeMap.put('BusinessName',BusinessNameType);         

                // newType3
                Map<String, String> newType3 = new Map<String, String> ();
                newType3.put('label','Phone');
                newType3.put('datatype','phone');
                labelTypeMap.put('Phone',newType3);     
                 
                // newType4
                Map<String, String> newType4 = new Map<String, String> ();
                newType4.put('label','SSN');
                newType4.put('datatype','text');
                labelTypeMap.put('SSN',newType4); 
                
                Map<String, String> newType5 = new Map<String, String> ();
                newType5.put('label','Alternate Id');
                newType5.put('datatype','text');
                labelTypeMap.put('AlternateId',newType5);       
           
                Map<String, Object> valueMap = request.searchValueMap;
                
                System.debug(' LookupRequest is '+request); 
                output.put('lookupRequest', request);
       
    }
    
    global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {    
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);
        } 
        
        else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
       Boolean success = true;      
       return success;
    }
    
    private list<AccountContactRelation> fetchAccountContForContact(Boolean singleContactAddressKeyword,Boolean onlyContactKeyword, Map<String,Object> searchValueMap){
    	List<AccountContactRelation> listToReturn = new List<AccountContactRelation>(); 
        Set<String> searchedSet = searchValueMap.keySet();
        String searchTypeSOSL = '';
        String searchTerms = '';
        String whereClauseForContact = '';
        Set<Id>ContactIds = new Set<Id>();
        String AccountContactRelationQuery = '';
        List<Contact> contactList = new List<Contact>();
        if(singleContactAddressKeyword || onlyContactKeyword){
            String searchedField = new List<String>(searchedSet)[0];
            searchTypeSOSL =  'FIND ';
            if(searchedField=='Name'){
                searchTypeSOSL+='\''+searchValueMap.get(searchedField)+'\' IN NAME FIELDS RETURNING Contact (Id)';		    
            }
            else if(searchedField=='Phone'){
                searchTypeSOSL += '\''+searchValueMap.get(searchedField)+'\' IN PHONE FIELDS RETURNING Contact (Id)';                    	    
            }
            else{
                searchTypeSOSL+= '\''+searchValueMap.get(searchedField)+'\' IN ALL FIELDS RETURNING Contact (Id)';                    	                            
            }
            
        }
        else if ((!singleContactAddressKeyword) && (!onlyContactKeyword)){
            
            if(searchedSet.contains('Address')){
                searchedSet.remove('Address');    
            }
            
            for(String st:searchedSet){
                if(!String.isEmpty(searchTerms)){
                    searchTerms = '("' + st;	    
                }
                else{
                    searchTerms += ' OR '+ st;    
                }
                searchTerms +='")';
                
                
                
                if(String.isNotEmpty(whereClauseForContact)){
                    whereClauseForContact+= ' AND ';    
                }
                if(st=='Name'){
                    whereClauseForContact+= ' Name ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                else if(st=='BusinessName'){
                    //whereClauseForContact+= ' Name ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                else if(st=='Phone'){
                    whereClauseForContact+= ' Phone ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                else if(st=='SSN'){
                    whereClauseForContact+= ' vlocity_cmt__SSN__c ='+'\''+searchValueMap.get(st)+'\'';		    
                }
                searchTypeSOSL = 'FIND '+searchTerms+'\' IN ALL FIELDS RETURNING Contact (Id)'+whereClauseForContact;
            }	    
        }
        if(!String.isEmpty(searchTypeSOSL)){
            List<List<sObject>> resultContacts = search.query(searchTypeSOSL);
            contactList = resultContacts[0];     
        }
                
        for(Contact ct:contactList){
            ContactIds.add(ct.Id);    
        }
        Map<String,AccountContactRelation> accConRelmap = new Map<String,AccountContactRelation>();
        String accountIdsExt = '';
        if(!ContactIds.isEmpty() && searchValueMap.keySet().contains('Address')){
            AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c,  Account.Customer_Class__c, Account.Customer_Class_Code__c FROM AccountContactRelation WHERE ';                        
            AccountContactRelationQuery+=  ' ContactId IN '+ ContactIds ;
            system.debug('AccountContactRelationQuery===='+AccountContactRelationQuery);
            listToReturn = Database.query(AccountContactRelationQuery);
            
        }
        return listToReturn;
    }
}