global with sharing class CC2020_IL_Account implements vlocity_cmt.VlocityOpenInterface2
{  

    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
            List<String> contextIds = (List<String>) inputs.get('contextIds');    
            String parentId = (String) inputs.get('parentId');           
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');            
            String lastRecordId = request.lastRecordId;   // R15 pagination support
            
            //get values entered by the user            
            Map<String,Object> searchValueMap = request.searchValueMap;   
            System.debug('@@@ searchValueMap : ' + searchValueMap);         
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');            
            Integer pageSize = 0; // R15 pagination support            
            Integer numRecords = 30; // R15 pagination support            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');           
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('AccountNo');
            resultFieldList.add('Name');
            resultFieldList.add('Address1');
            resultFieldList.add('Address2');
            
            List<String> verificationFieldList = new List<String> ();
            
            List<String> optionalVerificationFieldList = new List<String> ();
            
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();            
            Map<String, String> newType1 = new Map<String, String> ();            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Account#');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountNo', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Address1', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Address2', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Name', newType1);

            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            //String uniqueRequestName = 'Contact';
            
            //need to set bundle name manually so commenting below line
            //String drBundleName = (String) resultInfo.get('drBundleName');
            
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            //String interactionObjName = 'Contact';
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap = new Map<String, Object>();
              }
               // R15 pagination support
            else{

                Set<String> AccountIdExt = New Set<String>();
                List<Object> data = null;
                list<Service_Agreement__c> conList = new List<Service_Agreement__c>();
                //Map<String,List<Case>> AccountIdMap = New Map<String, List<Case>>();                
                Boolean dynamicPersonLableAdded = false;
                Boolean dynamicBusinessLableAdded = false;
                List<List<Account>> searchList = New List<List<Account>>();
                if(searchValueMap != null && !searchValueMap.isEmpty()) {      
                    System.debug('into ifff');
                    data = this.getAccount(searchValueMap);                 
                }
                
                if(data != null) {
                    for(Object con : data) {
                        Map<String, object> resultValueMap = ((Map<String, object>)con);
                        String recordId = String.valueOf(resultValueMap.get('RecordId'));
                        String drBundleName;
                        if(resultValueMap.get('Object') != null) {
                            drBundleName = String.valueOf(resultValueMap.get('Object')).equals('Account') ? 'CreateInteractionForAccount' : 'CreateInteractionForContact'; 
                        } else {
                            continue;
                        } 
                        vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);                        
                        result.setOptionalVerificationFieldList(optionalVerificationFieldList);                   
                        results.add(result);
                    }
                }
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
                List<vlocity_cmt.LookupResult> resultSubSet =          
                new List<vlocity_cmt.LookupResult>();
                Integer startIndex = 0;
            
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                      {
                            if(results[i].recordId == lastRecordId)
                            {
                                  startIndex = i + 1;
                            }
                        }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }            
                output.put('lookupResults', resultSubset);                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
         vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest'); 
         List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
         Map<String, Object> additionalData = request.additionalData;
         List<String> searchFieldList = request.searchFieldList;
         
         searchFieldList.add('SearchWithType');
         searchFieldList.add('AddressCompound');
         searchFieldList.add('IncludeInactive');
         
         //adding option for the field
         request.fieldPicklistValues.put('SearchWithType', new List<String> {'Account Number','SA Number','Meter Number', 'Landlord Aggrement No', 'Service Descriptor'});
         request.fieldPicklistValues.put('AddressCompound', new List<String> {'Premise','Mailing'});
         
         Map<String, Map<String, String>> labelTypeMap =  request.searchFieldsLabelTypeMap;
         Map<String, String> prop = new Map<String, String> ();
         prop.put('datatype','SearchWithType');
         prop.put('value1', 'searchType');
         prop.put('value2', 'searchValue');
         labelTypeMap.put('SearchWithType',prop);
         
         prop = new Map<String, String> ();
         prop.put('datatype','AddressCompound');
         prop.put('label1', 'Address');
         prop.put('value1', 'address');
         prop.put('value2', 'addressType');
         labelTypeMap.put('AddressCompound',prop);
         
         prop = new Map<String, String> ();
         prop.put('datatype','checkbox');
         prop.put('label', 'Include Inactive?');
         labelTypeMap.put('IncludeInactive',prop);

         Map<String, Object> valueMap = request.searchValueMap;
         output.put('lookupRequest', request);
    }
    
    global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {  
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);   
        } 
        
        else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
            
       // getSearchResults(methodName,output,options);
       Boolean success = true;     
       return success;
    }
    
    public List<Object> getAccount(Map<String,Object> searchValueMap) {
        List<Object> retData = new List<Object>();
        System.debug('into getAccount 1111');
        try {       
            String query = '';
            Boolean includeInactive = !(searchValueMap.get('IncludeInactive') == null || !((Boolean)searchValueMap.get('IncludeInactive')));
            String address=null;
            Set<String> s = null;
            Map<Id, vlocity_cmt__ServicePoint__c> accData = new Map<Id, vlocity_cmt__ServicePoint__c>();
            if(searchValueMap.containsKey('addressType') && searchValueMap.get('addressType') != '' && 
               searchValueMap.containsKey('address') && searchValueMap.get('address') != null ) {
                address = '%'+String.valueOf(searchValueMap.get('address'))+'%';
                s = new Set<String>();
                for(Object str : ((List<Object>)searchValueMap.get('addressType'))) {
                    s.add(String.valueOf(str));
                }
            }
            
            if(s != null && address != null) {
                if(s.contains('Mailing')) {
                    
                    query += ' Select AccountId, Account.Name, '; 
                    query += ' Account.Account_ID__c, Account.ParentId, ';
                    query += ' Account.Parent.Name, Account.Parent.Account_ID__c ';
                    query += ' From AccountContactRelation ';
                    //query += ' WHERE MAIN_CUST_SW__c = true ';
                    query += ' WHERE Contact_Mailing_Address__c LIKE :address ';
                    
                    System.debug('Final query for mailing query : ' + query);
                    List<AccountContactRelation> data = Database.query(query);
                    for(AccountContactRelation acr : data) {
                        if(!accData.containsKey(acr.AccountId)) {
                            //TODO: Need to fetch service point data for address. 
                            accData.put(acr.AccountId, null);
                        }
                    }
                    
                }
            }

            if((searchValueMap.containsKey('searchType') && searchValueMap.get('searchType') != '') ||
                s != null && !s.isEmpty()) {
            
                String whereClause = '';
                String searchVal = null;
                if(searchValueMap.containsKey('searchValue')) {
                    searchVal = String.valueOf(searchValueMap.get('searchValue'));
                }
                query = '';
                query += ' Select Service_Agreement__r.Account__c, Service_Agreement__r.Account_Name__c, '; 
                query += ' Service_Agreement__r.Account__r.Account_ID__c, Service_Agreement__r.Account__r.ParentId, ';
                query += ' Service_Agreement__r.Account__r.Parent.Name, Service_Agreement__r.Account__r.Parent.Account_ID__c, ';
                query += ' vlocity_cmt__PremisesId__r.Address__c, vlocity_cmt__PremisesId__r.Address_2__c, vlocity_cmt__PremisesId__r.Address_3__c, vlocity_cmt__PremisesId__r.Address_4__c, vlocity_cmt__PremisesId__r.vlocity_cmt__City__c, vlocity_cmt__PremisesId__r.County__c, vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c, vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c ';
                query += ' From vlocity_cmt__ServicePoint__c ';
                if(searchValueMap.containsKey('searchType') && searchValueMap.get('searchType') != '') {
                    if(String.valueOf(searchValueMap.get('searchType')).equals('Meter Number')) {
                        whereClause += ' Meter_Id__c = \'' + searchVal + '\'';
                    } else if(String.valueOf(searchValueMap.get('searchType')).equals('Landlord Aggrement No')) {
                        whereClause += ' vlocity_cmt__PremisesId__r.Landlord_Agreement_ID__c = \'' + searchVal + '\'';
                    } else if(String.valueOf(searchValueMap.get('searchType')).equals('Service Descriptor')) {
                        whereClause += ' vlocity_cmt__PremisesId__r.Service_Descriptor__c = \'' + searchVal + '\'';
                    } else if(String.valueOf(searchValueMap.get('searchType')).equals('Account Number')) {
                        whereClause += ' Service_Agreement__r.Account__r.Account_ID__c = \'' + searchVal + '\'';
                    } else if(String.valueOf(searchValueMap.get('searchType')).equals('SA Number')) {
                        whereClause += ' Service_Agreement__r.Service_Agreement_ID__c = \'' + searchVal + '\'';
                    }
                }
                
                List<String> status = new List<String> {'Pending Start', 'Active', 'Pending Stop', 'Stopped', 'Reactivated'};
                if(!includeInactive) {
                    if(whereClause != '')
                        whereClause += ' AND Service_Agreement__r.SA_Status__c IN :status ';
                    else
                        whereClause += ' Service_Agreement__r.SA_Status__c IN :status ';
                }
                
                Boolean bothAddress = false;
                if(s != null && s.contains('Premise') && address != null && !accData.isEmpty()) {
                    if( whereClause != '') {
                        whereClause = ' ( ( ' + whereClause;
                        bothAddress = true; 
                    }   
                }
                
                if(s != null && s.contains('Premise') && address != null) {
                    if(bothAddress) {
                        whereClause += ' ) AND ( vlocity_cmt__PremisesId__r.Consolidated_Address__c LIKE :address ';
                    } else {
                        if(whereClause != '')
                            whereClause += ' AND vlocity_cmt__PremisesId__r.Consolidated_Address__c LIKE :address ';
                        else
                            whereClause += ' vlocity_cmt__PremisesId__r.Consolidated_Address__c LIKE :address ';    
                    }
                    
                }   
                
                if(!accData.isEmpty()) {
                    Set<Id> accIds = accData.keySet();
                    if(bothAddress) {
                        whereClause += ' OR Service_Agreement__r.Account__c IN :accIds ';
                    } else {
                        if(whereClause != '') {
                            whereClause += ' AND Service_Agreement__r.Account__c IN :accIds ';
                        } else {
                            whereClause += ' Service_Agreement__r.Account__c IN :accIds ';
                        }
                    }
                }
                
                if(whereClause != '') {
                    whereClause = ' WHERE ' + whereClause;
                }
                if(bothAddress) {
                    whereClause += ' ) ) ';
                }
                System.debug('final query : ' + (query + whereClause));
                List<vlocity_cmt__ServicePoint__c> spData = Database.query(query + whereClause);
                for(vlocity_cmt__ServicePoint__c sp : spData) {
                    if(!accData.containsKey(sp.Service_Agreement__r.Account__c) || accData.get(sp.Service_Agreement__r.Account__c) == null) {
                        accData.put(sp.Service_Agreement__r.Account__c, sp);
                        System.debug('sp.Service_Agreement__r.Account__c : ' + sp.Service_Agreement__r.Account__c);
                        System.debug('sp.Service_Agreement__r.Account__c : ' + sp.Service_Agreement__r.Account__r.ParentId);
                    }
                }
            }
            System.debug('final accData : ' + accData);
            if(!accData.isEmpty()) {
                Set<Id> accIds = accData.keySet();
                query = '';
                query += ' Select '; 
                query += ' Contact.Name, ContactId, AccountId ';
                query += ' From AccountContactRelation ';
                query += ' WHERE MAIN_CUST_SW__c = true AND ';
                query += ' AccountId IN :accIds ';
                System.debug('final query for acr  : ' + query);
                List<AccountContactRelation> acr = Database.query(query); 
                Map<Id, AccountContactRelation> acrMap = new Map<Id, AccountContactRelation>();
                for(AccountContactRelation a : acr) {
                    acrMap.put(a.AccountId, a);
                }
                 
                Map<String, Object> record;
                for(String aId : accData.keySet()) {
                    record = new Map<String,Object>();
                    if(accData.get(aId) != null && accData.get(aId).Service_Agreement__r.Account__c != null) {
                        String add1 = (accData.get(aId).vlocity_cmt__PremisesId__r.Address__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.Address__c + ' ' : '') + (accData.get(aId).vlocity_cmt__PremisesId__r.Address_2__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.Address_2__c + ' ' : '') + (accData.get(aId).vlocity_cmt__PremisesId__r.Address_3__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.Address_3__c + ' ' : '') + (accData.get(aId).vlocity_cmt__PremisesId__r.Address_4__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.Address_4__c + ' ' : '');  
                        record.put('Address1', add1);
                        String add2 = (accData.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__City__c + ' ' : '') + (accData.get(aId).vlocity_cmt__PremisesId__r.County__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.County__c + ' ' : '') + (accData.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__Country__c + ' ' : '') + (accData.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c != null ? accData.get(aId).vlocity_cmt__PremisesId__r.vlocity_cmt__PostalCode__c + ' ' : '');
                        record.put('Address2', add2);
                        //B2B
                        if(accData.get(aId).Service_Agreement__r.Account__r.ParentId != null) {
                            record.put('RecordId', accData.get(aId).Service_Agreement__r.Account__r.ParentId);
                            record.put('AccountNo', accData.get(aId).Service_Agreement__r.Account__r.Parent.Account_ID__c);
                            record.put('Name', accData.get(aId).Service_Agreement__r.Account__r.Parent.Name);
                            record.put('Object', 'Account');
                        } else {
                            //B2C
                            if(acrMap.containsKey(aId)) {
                                record.put('RecordId', acrMap.get(aId).ContactId);
                                record.put('AccountNo', accData.get(aId).Service_Agreement__r.Account__r.Account_ID__c);
                                record.put('Name', acrMap.get(aId).Contact.Name);   
                                record.put('Object', 'Contact');
                            } else {
                                continue;
                            }
                        }
                    } else {
                        continue;
                    }
                    retData.add(record);
                }
            }
        } catch(Exception e) {
            System.debug('@@ exception : ' + e.getStackTraceString());  
            System.debug('@@ exception : ' + e.getMessage());
        }
        System.debug('data to be return : ' + retData);
        return retData;
    }

}