global with sharing class ILAccount implements vlocity_cmt.VlocityOpenInterface2
{  

    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
        {

            System.debug('ino getSearchResults inputs : ' + inputs);
            System.debug('ino getSearchResults output : ' + output);
            System.debug('ino getSearchResults options : ' + options);
            
            List<String> contextIds = (List<String>) inputs.get('contextIds');
    
            String parentId = (String) inputs.get('parentId');
            
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
            
            String lastRecordId = request.lastRecordId;   // R15 pagination support
            
            //get values entered by the user
            
            Map<String,Object> searchValueMap = request.searchValueMap;
            
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');
            
            Integer pageSize = 0; // R15 pagination support
            
            Integer numRecords = 30; // R15 pagination support
            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support
            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');
            
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;
            
           List<String> resultFieldList = new List<String> ();
            //resultFieldList.add('FirstName');
            //resultFieldList.add('LastName');
            resultFieldList.add('PersonName');
            resultFieldList.add('AccountId');
            resultFieldList.add('PersonId');
            //resultFieldList.add('Phone');
            //resultFieldList.add('Mailing_Address__c');
            //resultFieldList.add('Birthdate');       
            //resultFieldList.add('vlocity_cmt__SSN__c');
            //resultFieldList.add('vlocity_cmt__DriversLicenseNumber__c');
            //resultFieldList.add('Passport__c');
            //resultFieldList.add('Primises_Address__c');
            
            
             List<String> verificationFieldList = new List<String> ();
            verificationFieldList.add('PersonName'); 
            verificationFieldList.add('AccountId');
            
            List<String> optionalVerificationFieldList = new List<String> ();
            optionalVerificationFieldList.add('PersonId');
            
           
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();            
            Map<String, String> newType1 = new Map<String, String> ();            
            newType1.put('label','FirstName');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('FirstName', newType1);
           
            newType1 = new Map<String, String> ();            
            newType1.put('label','LastName');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('LastName', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Account Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Person Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Person Name');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonName', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Email');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Email', newType1);
            
            //Modified
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Phone');
            newType1.put('datatype','Phone');            
            resultFieldsLabelTypeMap.put('Phone', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Employment Status');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__EmploymentStatus__c', newType1); 
            
             newType1 = new Map<String, String> ();
             newType1.put('label', 'MailingAddress');
             newType1.put('datatype','Address');            
             resultFieldsLabelTypeMap.put('Mailing_Address__c', newType1);  
        
             newType1 = new Map<String, String> ();
             newType1.put('label', 'Birthdate');
             newType1.put('datatype','Date');            
             resultFieldsLabelTypeMap.put('Birthdate', newType1); 
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Occupation');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__Occupation__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Net Worth');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__NetWorth__c', newType1);
            
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Last 4 SSN');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__SSN__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Driver License');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Passport');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Passport__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Primises');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Primises_Address__c', newType1);
            
            
            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            String drBundleName = (String) resultInfo.get('drBundleName');
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap =
                new Map<String, Object>();
            
                resultValueMap.put('RelatedParty', 'John Smith');
                resultValueMap.put('Role', 'Carrier');
                resultValueMap.put('PartyName', 'John Smith');
                resultValueMap.put('Address', '50 Fremont, San Francisco, CA');
                
                String recordId = 'ExternalId';
            
                vlocity_cmt.LookupResult result =  new vlocity_cmt.LookupResult (resultFieldsLabelTypeMap,resultFieldList,resultValueMap, recordId, uniqueRequestName, request, verificationResult,verificationFieldList,drBundleName,interactionObjName);              
                result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                
                // R15 optional verification fields support
            
                results.add(result);
              }
               // R15 pagination support
            else{
                String whereClause = '';
                List<AccountContactRelation> data = null;
                list<Service_Agreement__c> conList = new List<Service_Agreement__c>();
                if(searchValueMap != null && !searchValueMap.isEmpty()) {
                    String query = ' Select Id, Account.Account_ID__c, Account_ID__c, Contact.Id, Contact.Name, Contact.Person_ID__c, Contact.FirstName, Contact.LastName, Contact.Phone, Contact.LeadSource, Contact.Birthdate, Contact.Mailing_Address__c, Contact.Primises_Address__c, Contact.Email, Contact.vlocity_cmt__EmploymentStatus__c, Contact.vlocity_cmt__Occupation__c, Contact.vlocity_cmt__NetWorth__c, Contact.vlocity_cmt__SSN__c, Contact.vlocity_cmt__DriversLicenseNumber__c, Contact.Passport__c From AccountContactRelation ';
                    for(String field : searchValueMap.keySet()) {
                        if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                            if(whereClause == '') {
                               whereClause += ' WHERE '; 
                            } else {
                                whereClause += ' AND ';
                            }
                            whereClause += field + ' = \'' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field))) +'\'';
                        }
                    }
                    
                    if(whereClause != '') {
                        query += whereClause;
                        System.debug('final query : ' + query);
                        data = Database.query(query);
                        /*if(SAs.size() > 0) {
                            Set<Id> accId = new Set<Id>();
                            for(Service_Agreement__c sa : SAs) {
                                accId.add(sa.Account__c);
                            }                           
                            data = [Select Id, Account.Account_ID__c, Account_Name__c, Contact.Id, Contact.Name, Contact.Person_ID__c, Contact.FirstName, Contact.LastName, Contact.Phone, Contact.LeadSource, Contact.Birthdate, Contact.Mailing_Address__c, Contact.Primises_Address__c, Contact.Email, Contact.vlocity_cmt__EmploymentStatus__c, Contact.vlocity_cmt__Occupation__c, Contact.vlocity_cmt__NetWorth__c, Contact.vlocity_cmt__SSN__c, Contact.vlocity_cmt__DriversLicenseNumber__c, Contact.Passport__c From AccountContactRelation Where AccountId IN :accId];
                        }*/
                        
                    }
                }
                
                //for(Contact con : Contacts)
               if(data != null) {
                    for(AccountContactRelation con : data) {
                   
                        System.debug('Sobjet==='+ con);
                        Map<String, object> resultValueMap = new Map<String, object>();
                        
                        
                        //resultValueMap.put('FirstName',con.Contact.FirstName != null ? con.Contact.FirstName : '');
                        //resultValueMap.put('LastName',con.Contact.LastName != null ? con.Contact.LastName : '');
                        resultValueMap.put('PersonName',con.Contact.Name != null ? con.Contact.Name : '');
                        resultValueMap.put('PersonId',con.Contact.Person_ID__c != null ? con.Contact.Person_ID__c : '');
                        resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');
                        
                        //resultValueMap.put('Email',con.Contact.Email != null ? con.Contact.Email : '');
                        //resultValueMap.put('Phone',con.Contact.Phone != null ? con.Contact.Phone : '');
                        //resultValueMap.put('Birthdate',con.Contact.Birthdate != null ? con.Contact.Birthdate : null);
                        //resultValueMap.put('Mailing_Address__c',con.Contact.Mailing_Address__c != null ? con.Contact.Mailing_Address__c : '');
                        //resultValueMap.put('Primises_Address__c',con.Contact.Primises_Address__c != null ? con.Contact.Primises_Address__c : '');
                        
                        //resultValueMap.put('vlocity_cmt__EmploymentStatus__c',con.Contact.vlocity_cmt__EmploymentStatus__c != null ? con.Contact.vlocity_cmt__EmploymentStatus__c : '');
                        //resultValueMap.put('vlocity_cmt__Occupation__c',con.Contact.vlocity_cmt__Occupation__c != null ? con.Contact.vlocity_cmt__Occupation__c : null);
                        //resultValueMap.put('vlocity_cmt__NetWorth__c',con.Contact.vlocity_cmt__NetWorth__c != null ? Double.valueOf(con.Contact.vlocity_cmt__NetWorth__c).format() : null);
                        //resultValueMap.put('vlocity_cmt__SSN__c',con.Contact.vlocity_cmt__SSN__c != null ? Double.valueOf(con.Contact.vlocity_cmt__SSN__c).format() : '');
                        //resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c',con.Contact.vlocity_cmt__DriversLicenseNumber__c != null ? con.Contact.vlocity_cmt__DriversLicenseNumber__c : '');
                        //resultValueMap.put('Passport__c',con.Contact.Passport__c != null ? con.Contact.Passport__c : '');
                        
    
                        String recordId = con.Contact.Id;  
                    
                        vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);         
                        
                        result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                   
                        results.add(result);
                    }
                }
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
                List<vlocity_cmt.LookupResult> resultSubSet =          
              new List<vlocity_cmt.LookupResult>();
            
                // results.sort();
                
                Integer startIndex = 0;
            
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                      {
                            if(results[i].recordId == lastRecordId)
                            {
                                  startIndex = i + 1;
                            }
                        }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }
            
                output.put('lookupResults', resultSubset);
                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                 // Logger.db('GetExternalSearchRequestResults
              // LookupResult   is
              // '+JSON.serializePretty(results));
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {
        
        
            System.debug('ino getSearchRequest inputs : ' + inputs);   
            System.debug('ino getSearchRequest output : ' + output);
            System.debug('ino getSearchRequest options : ' + options);
            
               vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
                
                List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
                
                Map<String, Object> additionalData = request.additionalData;     
                
                List<String> searchFieldList = request.searchFieldList;
                
                //searchFieldList.add('FirstName');
                //searchFieldList.add('LastName');
                searchFieldList.add('Account.Account_ID__c');
                //searchFieldList.add('Phone');
                //searchFieldList.add('PersonIds');
                /*searchFieldList.add('Mailing_Address__c');
                searchFieldList.add('Birthdate');
                searchFieldList.add('vlocity_cmt__SSN__c');
                searchFieldList.add('vlocity_cmt__DriversLicenseNumber__c');
                searchFieldList.add('Passport__c');
                searchFieldList.add('Primises_Address__c');*/
        
        
                
                 
               // searchFieldList.add('LeadSource');


                
                Map<String, Map<String, String>>labelTypeMap =   request.searchFieldsLabelTypeMap;
                
               // newType1
               /* Map<String, String> newType1 = new Map<String, String> ();
                newType1.put('label','FirstName');
                newType1.put('datatype','text');
                labelTypeMap.put('FirstName',newType1);*/
        
             // newType2
                Map<String, String> newType2 = new Map<String, String> ();
                newType2.put('label','Account Id');
                newType2.put('datatype','text');
                labelTypeMap.put('Account.Account_ID__c',newType2);        
                
                Map<String, Object> valueMap = request.searchValueMap;

                System.debug(' LookupRequest is '+request); 
                output.put('lookupRequest', request);
       
    }
global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
{
  
        
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);
         
         
        //  throw new AuraHandledException('ccccccccccccccccc');       
         
         } 
        
    else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
        
   // getSearchResults(methodName,output,options);
   Boolean success = true;
  
return success;
}
}