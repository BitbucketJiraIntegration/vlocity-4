global with sharing class GetMockupSearchRequestResultsCas implements vlocity_cmt.VlocityOpenInterface2
{  
    
    
    
    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
        {

            System.debug('ino getSearchResults inputs : ' + inputs);
            System.debug('ino getSearchResults output : ' + output);
            System.debug('ino getSearchResults options : ' + options);
            
            List<String> contextIds = (List<String>) inputs.get('contextIds');
    
            String parentId = (String) inputs.get('parentId');
            
            vlocity_cmt.LookupRequest request =
                (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
            
            String lastRecordId = request.lastRecordId; // R15 pagination support
            
            //get values entered by the user
            Map<String,Object> searchValueMap = request.searchValueMap;
            
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');
            
            Integer pageSize = 0; // R15 pagination support
            
            Integer numRecords = 30; // R15 pagination support
            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support
            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =
              vlocity_cmt__InteractionLauncherConfiguration__c.getInstance
                ('ClassBasedResultsPageSize');
            
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize =
              String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;
            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('FirstName');
            resultFieldList.add('LastName');
            resultFieldList.add('Phone');
            resultFieldList.add('Mailing_Address__c');
            resultFieldList.add('Birthdate');       
            resultFieldList.add('vlocity_cmt__SSN__c');
            resultFieldList.add('Driver_License__c');
            resultFieldList.add('Passport__c');
            resultFieldList.add('Primises_Address__c');
            
            
            List<String> verificationFieldList = new List<String> ();
         
            verificationFieldList.add('Email'); 
            verificationFieldList.add('Phone');
            verificationFieldList.add('Mailing_Address__c');
            verificationFieldList.add('Birthdate');
            verificationFieldList.add('vlocity_cmt__SSN__c');
           //verificationFieldList.add('vlocity_cmt__EmploymentStatus__c');
           verificationFieldList.add('Primises_Address__c');
            
            List<String> optionalVerificationFieldList = new List<String> ();
            
            optionalVerificationFieldList.add('vlocity_cmt__Occupation__c');
            optionalVerificationFieldList.add('vlocity_cmt__NetWorth__c');
            
           
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();
            
            Map<String, String> newType1 = new Map<String, String> ();            
            newType1.put('label','FirstName');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('FirstName', newType1);
           
            newType1 = new Map<String, String> ();            
            newType1.put('label','LastName');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('LastName', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Email');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Email', newType1);
            
            //Modified
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Phone');
            newType1.put('datatype','Phone');            
            resultFieldsLabelTypeMap.put('Phone', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Employment Status');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__EmploymentStatus__c', newType1); 
            
             newType1 = new Map<String, String> ();
             newType1.put('label', 'MailingAddress');
             newType1.put('datatype','Address');            
             resultFieldsLabelTypeMap.put('Mailing_Address__c', newType1);  
            
            /* newType1 = new Map<String, String> ();
             newType1.put('label', 'Lead Source');
             newType1.put('datatype','Picklist');            
             resultFieldsLabelTypeMap.put('Lead Source', newType1); */
        
             newType1 = new Map<String, String> ();
             newType1.put('label', 'Birthdate');
             newType1.put('datatype','Date');            
             resultFieldsLabelTypeMap.put('Birthdate', newType1); 
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Occupation');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__Occupation__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Net Worth');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__NetWorth__c', newType1);
            
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Last 4 SSN');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__SSN__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Driver License');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Driver_License__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Passport');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Passport__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Primises');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Primises_Address__c', newType1);
            
         
            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            String drBundleName = (String) resultInfo.get('drBundleName');
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap =
                new Map<String, Object>();
            
                resultValueMap.put('RelatedParty', 'John Smith');
                resultValueMap.put('Role', 'Carrier');
                resultValueMap.put('PartyName', 'John Smith');
                resultValueMap.put('Address', '50 Fremont, San Francisco, CA');
                
                String recordId = 'ExternalId';
            
                vlocity_cmt.LookupResult result =          
              new vlocity_cmt.LookupResult (resultFieldsLabelTypeMap,resultFieldList,
                 resultValueMap, recordId, uniqueRequestName, request, verificationResult,
                 verificationFieldList,drBundleName,interactionObjName);         
                
                result.setOptionalVerificationFieldList(optionalVerificationFieldList);
            // R15 optional verification fields support
            
                results.add(result);
            }
            // R15 pagination support
            else{
                List<Contact> contacts = new List<Contact>();
                
                if(searchValueMap != null && !searchValueMap.isEmpty()) {
                   
                    String query = 'Select Id, Name, FirstName, LastName,Phone,LeadSource,Birthdate,Mailing_Address__c, Primises_Address__c, Email, vlocity_cmt__EmploymentStatus__c, vlocity_cmt__Occupation__c,vlocity_cmt__NetWorth__c,vlocity_cmt__SSN__c,Driver_License__c,Passport__c From Contact ';
                    
                   // List<List<SObject>> query='[FIND IN ALL FIELDS RETURNING Contact(Id, Name, FirstName, LastName,Phone,LeadSource,Birthdate,Mailing_Address__c, Primises_Address__c, Email, vlocity_cmt__EmploymentStatus__c, vlocity_cmt__Occupation__c,vlocity_cmt__NetWorth__c,vlocity_cmt__SSN__c,vlocity_cmt__DriversLicenseNumber__c,Passport__c),Contact]';
                   
                    //List<List<SObject>> searchList = [FIND 'SFDC' IN ALL FIELDS RETURNING Account(Name), Contact(FirstName,LastName)];
                    
                    String whereClause = '';
                    for(String field : searchValueMap.keySet()) {
                        if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                            /**********************/
                            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                            Schema.SObjectType leadSchema = schemaMap.get('Contact');
                            
                            Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
                            Schema.DisplayType fielddataType = fieldMap.get(field).getDescribe().getType();
                            String fieldName = fieldMap.get(field).getDescribe().getName();
                            System.debug('Fieldname'+ fieldName);
                            System.debug('fielddataType'+ fielddataType);
                            if(whereClause == ''){
                                whereClause += ' WHERE ';
                            }
                            else{
                                whereClause += ' AND ';
                            }
                            
                            if(fielddataType == Schema.DisplayType.String && fieldName.equalsIgnoreCase('vlocity_cmt__ssn__c')){
                                try 
                                {
                                	string lst4Chr = (String.valueOf(searchValueMap.get(field))).trim().right(4);
                                	whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(lst4Chr) +'\'';
                                }
                                catch(Exception e){ }
                            }
                      
                            /*else if(fielddataType == Schema.DisplayType.TEXTAREA && fieldName.equalsIgnoreCase('Driver_License__c'))
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                            
                            else if(fielddataType == Schema.DisplayType.TEXTAREA  && fieldName.equalsIgnoreCase('Passport__c'))
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' '; */
                            
                             else if(fielddataType == Schema.DisplayType.TEXTAREA)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                          
                            else if(fielddataType == Schema.DisplayType.String)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                           
                            else if(fielddataType == Schema.DisplayType.PHONE)
                                whereClause += field + ' LIKE \'%' + String.valueOf(searchValueMap.get(field)).trim()+'%\'';
            
                            else if(fielddataType == Schema.DisplayType.ADDRESS)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                            
                            else if(fielddataType == Schema.DisplayType.DATE)
                                whereClause += field + ' = ' + String.valueOf(searchValueMap.get(field)).trim();
                            
                            /* else if(fielddataType == Schema.DisplayType.PICKLIST)
                                whereClause += field + ' CONTAINS \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';   */                      
                               
                          
                         
                            /*   else if(fielddataType == Schema.DisplayType.TEXTAREA)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '\'';      
                            
                            else if(fielddataType == Schema.DisplayType.TEXTAREA)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                    
                          else if(fielddataType == Schema.DisplayType.TEXTAREA)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                            */
                            
                            /*else if(fielddataType == Schema.DisplayType.REFERENCE)
                                whereClause += field + ' LIKE \'%e
' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';*/
                            /**********************/
                            /*if(whereClause == '') {
                                whereClause += ' WHERE ' + field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                            } else {
                                whereClause += ' AND ' + field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                            }*/
                        }
                    }
                        
                    if(whereClause != '') {
                        query += whereClause;
                        System.debug('final query : ' + query);
                        contacts = Database.query(query);
                    }
                }
                
                for(Contact con : contacts)
                {
                    Map<String, Object> resultValueMap = new Map<String, Object>();
                    
                    resultValueMap.put('FirstName',con.get('FirstName') != null ? con.get('FirstName') : '');
                    resultValueMap.put('LastName',con.get('LastName') != null ? con.get('LastName') : '');
                    resultValueMap.put('Email',con.get('Email') != null ? con.get('Email') : '');
                    resultValueMap.put('Phone',con.get('Phone') != null ? con.get('Phone') : '');
                    resultValueMap.put('Birthdate',con.get('Birthdate') != null ? con.get('Birthdate') : '');
                    resultValueMap.put('Mailing_Address__c',con.get('Mailing_Address__c') != null ? con.get('Mailing_Address__c') : '');
                    resultValueMap.put('Primises_Address__c',con.get('Primises_Address__c') != null ? con.get('Primises_Address__c') : '');
                  //  resultValueMap.put('LeadSource',con.get('LeadSource') != null ? con.get('LeadSource') : '');
                    
                    resultValueMap.put('vlocity_cmt__EmploymentStatus__c',con.get('vlocity_cmt__EmploymentStatus__c') != null ? con.get('vlocity_cmt__EmploymentStatus__c') : '');
                    resultValueMap.put('vlocity_cmt__Occupation__c',con.get('vlocity_cmt__Occupation__c') != null ? con.get('vlocity_cmt__Occupation__c') : '');
                    resultValueMap.put('vlocity_cmt__NetWorth__c',con.get('vlocity_cmt__NetWorth__c') != null ? Double.valueOf(con.get('vlocity_cmt__NetWorth__c')).format() : '');
                    resultValueMap.put('vlocity_cmt__SSN__c',con.get('vlocity_cmt__SSN__c') != null ? Double.valueOf(con.get('vlocity_cmt__SSN__c')).format() : '');
                    resultValueMap.put('Driver_License__c',con.get('Driver_License__c') != null ? con.get('Driver_License__c') : '');
                    resultValueMap.put('Passport__c',con.get('Passport__c') != null ? con.get('Passport__c') : '');
                    
                    //String recordId = 'ExternalId'+ String.valueOf(k);
                    String recordId = String.valueOf(con.get('Id'));
                
                    vlocity_cmt.LookupResult result =
                            new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList,
                resultValueMap, recordId, uniqueRequestName, request,
                verificationResult,verificationFieldList,drBundleName, interactionObjName);         
                    
                    result.setOptionalVerificationFieldList(                optionalVerificationFieldList);
               
                    results.add(result);
                }
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
                List<vlocity_cmt.LookupResult> resultSubSet =          
              new List<vlocity_cmt.LookupResult>();
            
                // results.sort();
                
                Integer startIndex = 0;
            
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                      {
                            if(results[i].recordId == lastRecordId)
                            {
                                  startIndex = i + 1;
                            }
                        }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }
            
                output.put('lookupResults', resultSubset);
                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                 // Logger.db('GetExternalSearchRequestResults
              // LookupResult   is
              // '+JSON.serializePretty(results));
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {
            System.debug('ino getSearchRequest inputs : ' + inputs);
            System.debug('ino getSearchRequest output : ' + output);
            System.debug('ino getSearchRequest options : ' + options);
            
               vlocity_cmt.LookupRequest request =
                (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
                
                List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
                
                Map<String, Object> additionalData = request.additionalData;     
                
                List<String> searchFieldList = request.searchFieldList;
                
        		searchFieldList.add('FirstName');
                searchFieldList.add('LastName');
                searchFieldList.add('Phone');
                searchFieldList.add('Mailing_Address__c');
                searchFieldList.add('Birthdate');
                searchFieldList.add('vlocity_cmt__SSN__c');
        		searchFieldList.add('Driver_License__c');
        		searchFieldList.add('Passport__c');
        		searchFieldList.add('Primises_Address__c');
        
                
                 
       		   // searchFieldList.add('LeadSource');


                
                Map<String, Map<String, String>>labelTypeMap =   request.searchFieldsLabelTypeMap;
                
               // newType1
        		Map<String, String> newType1 = new Map<String, String> ();
                newType1.put('label','FirstName');
                newType1.put('datatype','text');
                labelTypeMap.put('FirstName',newType1);
        
        	 // newType2
                Map<String, String> newType2 = new Map<String, String> ();
                newType2.put('label','LastName');
                newType2.put('datatype','text');
                labelTypeMap.put('LastName',newType2);
        
                
             // newType3
                Map<String, String> newType3 = new Map<String, String> ();
                newType3.put('label','Phone');
                newType3.put('datatype','Phone');
                labelTypeMap.put('Phone',newType3);     
                 
             // newType4
                Map<String, String> newType4 = new Map<String, String> ();
                 newType4.put('label','MailingAddress');
                 newType4.put('datatype','Address');
                 labelTypeMap.put('Mailing_Address__c',newType4); 
        
        
              // newType5
                Map<String, String> newType5 = new Map<String, String> ();
                  newType5.put('label','Birthdate');
                  newType5.put('datatype','Date');
                  labelTypeMap.put('Birthdate',newType5); 
      		 
             // newType6
                Map<String, String> newType6 = new Map<String, String> ();
                  newType6.put('label','Last 4 SSN');
                  newType6.put('datatype','text');
                  labelTypeMap.put('vlocity_cmt__SSN__c',newType6);
        	 
            // newType7
                Map<String, String> newType7 = new Map<String, String> ();
                  newType7.put('label','Driver License ');
                  newType7.put('datatype','text');
                  labelTypeMap.put('Driver_License__c',newType7);
        
                 
      	  // newType8
      	  
                Map<String, String> newType8 = new Map<String, String> ();
                  newType8.put('label','Passport');
                  newType8.put('datatype','text');
                  labelTypeMap.put('Passport__c',newType8);
        
        		Map<String, String> newType9 = new Map<String, String> ();
                 newType9.put('label','Primises');
                 newType9.put('datatype','text');
                 labelTypeMap.put('Primises_Address__c',newType9); 
        
        // newType9
       			 
            /*	 Map<String, String> newType6 = new Map<String, String> ();
                   newType6.put('label',' LeadSource');
                   newType6.put('datatype','Picklist');
                    labelTypeMap.put('LeadSource',newType6); */
        
                
                Map<String, Object> valueMap = request.searchValueMap;
                
                //valueMap.put('FirstName',null);
              //  valueMap.put('LastName',null);
                
                System.debug(' LookupRequest is '+request); 
                output.put('lookupRequest', request);
       
    }
global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
{
  
        
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);
         } 
        
    else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
        
   // getSearchResults(methodName,output,options);
   Boolean success = true;
  
return success;
}
}