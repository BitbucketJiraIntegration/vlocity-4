global with sharing class GetMockupSearchRequestResults implements vlocity_cmt.VlocityOpenInterface2
{  

    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
        {

            System.debug('ino getSearchResults inputs : ' + inputs);
            System.debug('ino getSearchResults output : ' + output);
            System.debug('ino getSearchResults options : ' + options);
            
            List<String> contextIds = (List<String>) inputs.get('contextIds');
    
            String parentId = (String) inputs.get('parentId');
            
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
            
            String lastRecordId = request.lastRecordId;   // R15 pagination support
            
            //get values entered by the user
            
            Map<String,Object> searchValueMap = request.searchValueMap;
            
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');
            
            Integer pageSize = 0; // R15 pagination support
            
            Integer numRecords = 30; // R15 pagination support
            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support
            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');
            
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;
            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('PersonName');
            resultFieldList.add('AccountId');
            resultFieldList.add('PersonId');
            
            List<String> verificationFieldList = new List<String> ();
            verificationFieldList.add('PersonName'); 
            
            List<String> optionalVerificationFieldList = new List<String> ();
            optionalVerificationFieldList.add('AccountId');
            optionalVerificationFieldList.add('PersonId');
            
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();
            
            Map<String, String> newType1 = new Map<String, String> ();            
            newType1.put('label','Account Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Person Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Person Name');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonName', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Email');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Email', newType1);
            
            //modified
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Phone');
            newType1.put('datatype','Phone');            
            resultFieldsLabelTypeMap.put('Phone', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Employment Status');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__EmploymentStatus__c', newType1); 
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'MailingAddress');
            newType1.put('datatype','Address');            
            resultFieldsLabelTypeMap.put('Mailing_Address__c', newType1);  
            
            /* newType1 = new Map<String, String> ();
            newType1.put('label', 'Lead Source');
            newType1.put('datatype','Picklist');            
            resultFieldsLabelTypeMap.put('Lead Source', newType1); */
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Birthdate');
            newType1.put('datatype','Date');            
            resultFieldsLabelTypeMap.put('Birthdate', newType1); 
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Occupation');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__Occupation__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Net Worth');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__NetWorth__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Last 4 SSN');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__SSN__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Driver License');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Passport');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Passport__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Primises');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('Primises_Address__c', newType1);
            
            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            String drBundleName = (String) resultInfo.get('drBundleName');
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap =
                new Map<String, Object>();
            
                resultValueMap.put('RelatedParty', 'John Smith');
                resultValueMap.put('Role', 'Carrier');
                resultValueMap.put('PartyName', 'John Smith');
                resultValueMap.put('Address', '50 Fremont, San Francisco, CA');
                
                String recordId = 'ExternalId';
            
                vlocity_cmt.LookupResult result =  new vlocity_cmt.LookupResult (resultFieldsLabelTypeMap,resultFieldList,resultValueMap, recordId, uniqueRequestName, request, verificationResult,verificationFieldList,drBundleName,interactionObjName);              
                result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                
                // R15 optional verification fields support
            
                results.add(result);
              }
               // R15 pagination support
            else{
               //  List<Contact> contacts = new List<Contact>();
                
               //List<list<SObject>> contacts = new List<List<SObject>>();
                
                list<Contact> conList = new List<Contact>();
                if(searchValueMap != null && !searchValueMap.isEmpty()) {

                    String queryString='';
                    
                    String whereClause = '';
                
                    System.debug('@@searchValueMap : ' + searchValueMap);
                    
                    String valueSearch = '';
                    
                    for(String field : searchValueMap.keySet()) {
                        if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                            
                            if(valueSearch == '') {
                                valueSearch += ' {"' + searchValueMap.get(field) + '" ';
                            } else {
                                valueSearch += ' AND "' + searchValueMap.get(field) + '"';
                            }
                            /**********************/
                            /*Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                            Schema.SObjectType leadSchema = schemaMap.get('Contact');
                            
                            Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
                            Schema.DisplayType fielddataType = fieldMap.get(field).getDescribe().getType();
                            String fieldName = fieldMap.get(field).getDescribe().getName();
                            
                            System.debug('Fieldname'+ fieldName);
                            System.debug('fielddataType'+ fielddataType);
                          
                             if(whereClause == ''){
                                whereClause += ' WHERE ';
                            }
                            else{
                                whereClause += ' AND ';
                            } 
                            
                            if(fielddataType == Schema.DisplayType.String && fieldName.equalsIgnoreCase('vlocity_cmt__ssn__c')){
                                try 
                                {
                                    String lst4Chr = (String.valueOf(searchValueMap.get(field))).trim().right(4);
                                    whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(lst4Chr) +'\'';
                                }
                                catch(Exception e){ }
                            }
     
                             else if(fielddataType == Schema.DisplayType.TEXTAREA)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                          
                            else if(fielddataType == Schema.DisplayType.String)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                           
                            else if(fielddataType == Schema.DisplayType.PHONE)
                                whereClause += field + ' LIKE \'%' + String.valueOf(searchValueMap.get(field)).trim()+'%\'';
            
                            else if(fielddataType == Schema.DisplayType.ADDRESS)
                                whereClause += field + ' LIKE \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)).trim()) + '%\' ';
                            
                            else if(fielddataType == Schema.DisplayType.DATE)
                                whereClause += field + ' = ' + String.valueOf(searchValueMap.get(field)).trim();
                            */
                            
                        }
                    }
                        
                    if(valueSearch != '') {
                        valueSearch += '} ';
                        queryString = 'FIND ' + valueSearch + ' IN ALL FIELDS RETURNING Contact(Id, Name, FirstName, LastName, Person_ID__c, Account.Account_ID__c, Phone,LeadSource,Birthdate,Mailing_Address__c, Primises_Address__c, Email, vlocity_cmt__EmploymentStatus__c, vlocity_cmt__Occupation__c,vlocity_cmt__NetWorth__c,vlocity_cmt__SSN__c,vlocity_cmt__DriversLicenseNumber__c,Passport__c ';
                        if(parentId != null && parentId != '') {
                            queryString += ' where AccountId = :parentId ';
                        }
                        queryString += ' ) ';
                        System.debug('final query : ' + queryString);
                        List<List<SObject>> contacts = search.query(queryString );
                        conList  = ((List<contact>)contacts[0]);
                        System.debug('@@ contacts : ' + contacts);
                    }
                }
                
                //for(Contact con : Contacts)
               
                for(Contact con : conList)
                    
                {
               
                    System.debug('Sobjet==='+ con);
                  //  for(List<sObject> con : Query)
                       
                      Map<String, object> resultValueMap = new Map<String, object>();
                      //Map<String, List<SObject>> resultValueMap = new Map<String, List<SObject>>();
                     // Map<String, String> resultValueMap = new Map<String, String>();
                    
                    resultValueMap.put('PersonName',con.Name != null ? con.Name : '');
                    resultValueMap.put('PersonId',con.Person_ID__c != null ? con.Person_ID__c : '');
                    resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');

                    String recordId = String.valueOf(con.get('Id'));  
                  
                
                    vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);         
                    
                    result.setOptionalVerificationFieldList(optionalVerificationFieldList);
               
                    results.add(result);
                }
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
                List<vlocity_cmt.LookupResult> resultSubSet =          
              new List<vlocity_cmt.LookupResult>();
            
                // results.sort();
                
                Integer startIndex = 0;
            
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                      {
                            if(results[i].recordId == lastRecordId)
                            {
                                  startIndex = i + 1;
                            }
                        }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }
            
                output.put('lookupResults', resultSubset);
                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                 // Logger.db('GetExternalSearchRequestResults
              // LookupResult   is
              // '+JSON.serializePretty(results));
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {
        
        
            System.debug('ino getSearchRequest inputs : ' + inputs);   
            System.debug('ino getSearchRequest output : ' + output);
            System.debug('ino getSearchRequest options : ' + options);
            
               vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');
                
                List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
                
                Map<String, Object> additionalData = request.additionalData;     
                
                List<String> searchFieldList = request.searchFieldList;
                
                //searchFieldList.add('FirstName');
                //searchFieldList.add('LastName');
                searchFieldList.add('Name');
                searchFieldList.add('Phone');
                searchFieldList.add('PersonIds');
                searchFieldList.add('MailingAddress');
                /*searchFieldList.add('Mailing_Address__c');
                searchFieldList.add('Birthdate');
                searchFieldList.add('vlocity_cmt__SSN__c');
                searchFieldList.add('vlocity_cmt__DriversLicenseNumber__c');
                searchFieldList.add('Passport__c');
                searchFieldList.add('Primises_Address__c');*/
        
        
                
                 
               // searchFieldList.add('LeadSource');


                
                Map<String, Map<String, String>>labelTypeMap =   request.searchFieldsLabelTypeMap;
                
               // newType1
               /* Map<String, String> newType1 = new Map<String, String> ();
                newType1.put('label','FirstName');
                newType1.put('datatype','text');
                labelTypeMap.put('FirstName',newType1);*/
        
             // newType2
                Map<String, String> newType2 = new Map<String, String> ();
                newType2.put('label','Name');
                newType2.put('datatype','text');
                labelTypeMap.put('Name',newType2);
        
                
             // newType3
                Map<String, String> newType3 = new Map<String, String> ();
                newType3.put('label','Person Phone');
                newType3.put('datatype','Phone');
                labelTypeMap.put('Phone',newType3);     
                 
             // newType4
                Map<String, String> newType4 = new Map<String, String> ();
                newType4.put('label','Person Identifiers');
                newType4.put('datatype','Phone');
                labelTypeMap.put('PersonIds',newType4); 
                
                Map<String, String> newType5 = new Map<String, String> ();
                newType5.put('label','Mailing Address');
                newType5.put('datatype','text');
                labelTypeMap.put('MailingAddress',newType5); 
                 
        
           
                Map<String, Object> valueMap = request.searchValueMap;
                
                //valueMap.put('FirstName',null);
              //  valueMap.put('LastName',null);
                
                System.debug(' LookupRequest is '+request); 
                output.put('lookupRequest', request);
       
    }
global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
{
  
        
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);
         
         
        //  throw new AuraHandledException('ccccccccccccccccc');       
         
         } 
        
    else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
        
   // getSearchResults(methodName,output,options);
   Boolean success = true;
  
return success;
}
}