global with sharing class CCB_ILCase implements vlocity_cmt.VlocityOpenInterface2
{  

    global  void getSearchResults(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
        {
               
            List<String> contextIds = (List<String>) inputs.get('contextIds');    
            String parentId = (String) inputs.get('parentId');           
            vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest');            
            String lastRecordId = request.lastRecordId;   // R15 pagination support
            
            //get values entered by the user            
            Map<String,Object> searchValueMap = request.searchValueMap;            
            Map<String, Object> resultInfo = (Map<String, Object>) inputs.get('resultInfo');            
            Integer pageSize = 0; // R15 pagination support            
            Integer numRecords = 30; // R15 pagination support            
            // R15 pagination support
            // get ClassBasedResultsPageSize from customSetting
            // R15 pagination support            
            vlocity_cmt__InteractionLauncherConfiguration__c myCS =vlocity_cmt__InteractionLauncherConfiguration__c.getInstance ('ClassBasedResultsPageSize');           
            if((myCS != null) && (myCS.vlocity_cmt__IsFeatureOn__c))
            {
                pageSize = String.isNotBlank(myCS.vlocity_cmt__DisplayMessage__c) ?
                Integer.valueOf (myCS.vlocity_cmt__DisplayMessage__c) : 10;
            }
            
            Boolean hasMore = false;            
            List<String> resultFieldList = new List<String> ();
            resultFieldList.add('AccountId');
            resultFieldList.add('PersonId');
            resultFieldList.add('PersonName');
            //resultFieldList.add('BusinessName');
            resultFieldList.add('RelationshipType');//currently taken as record type
           	resultFieldList.add('Phone');// couldn't get the field 	Primary Phone Number (Phone Type, Phone Number) so taken Phone field
            //resultFieldList.add('vlocity_cmt__DriversLicenseNumber__c');//this is written inside check condition of record type
            //resultFieldList.add('vlocity_cmt__TaxId__c');//this is written inside check condition of record type
            resultFieldList.add('CaseId');
            resultFieldList.add('CaseStatus');
  
            List<String> verificationFieldList = new List<String> ();
            verificationFieldList.add('PersonName'); 
            verificationFieldList.add('AccountId');
            
            List<String> optionalVerificationFieldList = new List<String> ();
            optionalVerificationFieldList.add('PersonId');
            
            Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>> ();            
            
            Map<String, String> newType1 = new Map<String, String> ();            
           
            newType1 = new Map<String, String> ();            
            newType1.put('label','Account Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('AccountId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Person Id');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonId', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Name');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('PersonName', newType1);
            
            newType1 = new Map<String, String> ();            
            newType1.put('label','Account Relationship Type');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('RelationshipType', newType1);
           
            
            //Modified
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Phone');
            newType1.put('datatype','Phone');            
            resultFieldsLabelTypeMap.put('Phone', newType1);
 
            //below code is shifted inside recordtype check    
            /*newType1 = new Map<String, String> ();
            newType1.put('label', 'Driver\'s License Number');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'TIN');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('vlocity_cmt__TaxId__c', newType1);*/
            
            
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Case ID');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('CaseId', newType1);
            
            newType1 = new Map<String, String> ();
            newType1.put('label', 'Case Status');
            newType1.put('datatype','text');            
            resultFieldsLabelTypeMap.put('CaseStatus', newType1);
            
            Map<String, Object> verificationResult = new Map<String, Object>();        
            
            String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
            String drBundleName = (String) resultInfo.get('drBundleName');
            String interactionObjName = (String) resultInfo.get('interactionObjName');
            
            List<vlocity_cmt.LookupResult> results = new List<vlocity_cmt.LookupResult> ();
            
            //TODO::Need to understand contextId logic
            if((contextIds != null) && (contextIds.size()==1)) {
                Map<String, Object> resultValueMap = new Map<String, Object>();
           
              }
               // R15 pagination support
            else{
                String query ='';
                String whereClause = '';
                String searchedValue = '';
                Map<String,List<Case>> AccountIdMap = New Map<String, List<Case>>();                
                Boolean dynamicPersonLableAdded = false;
                Boolean dynamicBusinessLableAdded = false;
                List<List<Case>> searchList = New List<List<Case>>();
                List<AccountContactRelation> data = null;
                if(searchValueMap != null && !searchValueMap.isEmpty()) {             	
                    for(String field : searchValueMap.keySet()){
                        if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                            searchedValue = String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field)));
                        }
                    }
                        
                    if(String.isNotBlank(searchedValue)){
                                                         String searchValueFind = searchedValue+'*';                                
                                                         searchList = [FIND :searchValueFind IN ALL FIELDS RETURNING 
                                                                       Case  (AccountId, status,Case_Id__c WHERE Case_Id__c LIKE :'%'+searchedValue+'%')];	    
                                                        }
                        system.debug('searchList===='+searchList);
                        for(List<Case> sl:searchList){                        	
                            for(Case insideList:sl){
                                if(AccountIdMap.containsKey(insideList.AccountId)) {
                                    List<Case> casesList = AccountIdMap.get(insideList.AccountId);
                                    casesList.add(insideList);
                                    AccountIdMap.put(insideList.AccountId, casesList);
                                } else {
                                    AccountIdMap.put(insideList.AccountId, new List<Case> { insideList });
                                }
                            }
                           
                        }
                        if(!AccountIdMap.isEmpty()){
                            List<Account> Accounts = [Select Id,Account_ID__c from Account WHERE ID IN : AccountIdMap.keySet()]; 
                            String InQueryClause = '';
                            List<Case> caseNumbers;
                            for(Account acc:Accounts){
                                caseNumbers =  New List<Case>();
                                caseNumbers.addAll(AccountIdMap.get(acc.Id));
                                AccountIdMap.remove(acc.Id);
                                AccountIdMap.put(acc.Account_ID__c,caseNumbers);
                                if(String.isNotEmpty(InQueryClause)){
                                 	InQueryClause = InQueryClause+'\''+acc.Account_ID__c+'\',';    
                                 }
                                 else{
                                 	InQueryClause = '('+'\''+acc.Account_ID__c+'\',';    
                                 }
                            		    
                            }
                             String AccountContactRelationQuery ='';
                            if(String.isNotBlank(InQueryClause)){
                                InQueryClause = InQueryClause.removeEnd(',');
                                InQueryClause = InQueryClause+')';	    
                        	}
                            if(String.isNotBlank(InQueryClause)){
                                AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c, Contact.Phone,  Contact.vlocity_cmt__DriversLicenseNumber__c, Contact.recordtype.name,Contact.vlocity_cmt__TaxId__c, Account.Customer_Class__c, Account.Customer_Class_Code__c FROM AccountContactRelation WHERE MAIN_CUST_SW__c = true AND Account.Account_ID__c IN '+ InQueryClause ;
                                data = Database.query(AccountContactRelationQuery);                       		    
                            }
                        }
                    }
                	
                 if(data != null) {
                    for(AccountContactRelation con : data) { 
                        System.debug(AccountIdMap.keySet()+'Sobjet==='+ con.Account.Account_ID__c);
                        for(Case st:AccountIdMap.get(con.Account.Account_ID__c)){System.debug('st==='+ st);
                            Map<String, object> resultValueMap = new Map<String, object>();
                            resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');
                            resultValueMap.put('PersonId',con.Contact.Person_ID__c != null ? con.Contact.Person_ID__c : '');
                            resultValueMap.put('PersonName',con.Contact.Name != null ? con.Contact.Name : '');
                            resultValueMap.put('CaseId',st.Case_Id__c != null ? st.Case_Id__c : '');
                            resultValueMap.put('CaseStatus',st.status != null ? st.status : '');
                            if('RES'.equalsIgnoreCase(con.Account.Customer_Class_Code__c.trim())){
                               resultValueMap.put('RelationshipType','Residential');
                                if(!dynamicPersonLableAdded){
                                    resultFieldList.add('vlocity_cmt__DriversLicenseNumber__c');
                                    newType1 = new Map<String, String> ();
                                    newType1.put('label', 'Driver\'s License Number');
                                    newType1.put('datatype','text');            
                                    resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
                                    dynamicPersonLableAdded = true;
                                }
                                resultValueMap.put('vlocity_cmt__TaxId__c',''); 
                                resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c',con.Contact.vlocity_cmt__DriversLicenseNumber__c != null ? con.Contact.vlocity_cmt__DriversLicenseNumber__c : '');    
                            }
                             else {
                                if(!'RES'.equalsIgnoreCase(con.Account.Customer_Class_Code__c.trim()) && String.isNotBlank(con.Account.Customer_Class_Code__c)){
                                	resultValueMap.put('RelationshipType','Non-Residential' );
                                }
                                else{
                                	resultValueMap.put('RelationshipType','' );     
                                }
                                if(!dynamicBusinessLableAdded){
                                    resultFieldList.add('vlocity_cmt__TaxId__c');                               
                                    newType1 = new Map<String, String> ();
                                    newType1.put('label', 'TIN');
                                    newType1.put('datatype','text');            
                                    resultFieldsLabelTypeMap.put('vlocity_cmt__TaxId__c', newType1);
                                    dynamicBusinessLableAdded = true;
                                }
                                resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c','');   
                                resultValueMap.put('vlocity_cmt__TaxId__c',con.Contact.vlocity_cmt__TaxId__c != null ? con.Contact.vlocity_cmt__TaxId__c : '');    
                            } 
                             
                            String recordId = con.Contact.Id;  
                            vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);
                            result.setOptionalVerificationFieldList(optionalVerificationFieldList);
                            results.add(result);
                        }
                    }
               }
                
                
                
                
                
                
                
                /*
                system.debug('searchValueMap!!!!'+searchValueMap);
                String whereClauseCase = '';
                String InQueryClause = '';
                String caseNumber = null;
                String caseStatus = null;
                Boolean dynamicPersonLableAdded = false;
                Boolean dynamicBusinessLableAdded = false;
                List<AccountContactRelation> data = null;
                List<Case> CaseAccounts = New List<Case>();
                Map<String,Case> CaseAccountMap = New Map<String,Case>();
                list<Service_Agreement__c> conList = new List<Service_Agreement__c>();
                if(searchValueMap != null && !searchValueMap.isEmpty()) {
                    
                    String caseQuery = 'Select Account.Account_ID__c, status,CaseNumber FROM Case';
                    for(String field : searchValueMap.keySet()) {
                        if(searchValueMap.get(field) != null && !String.valueOf(searchValueMap.get(field)).trim().equals('')) {
                            if(whereClauseCase == '') {
                               whereClauseCase += ' WHERE '; 
                            } else {
                                whereClauseCase += ' AND ';
                            }
                            whereClauseCase += field + ' LIKE  \'%' + String.escapeSingleQuotes(String.valueOf(searchValueMap.get(field))) +'%\'';
                        }
                    }
                   if(String.isNotBlank(whereClauseCase)) {                       
                        caseQuery += whereClauseCase;
                        System.debug('final query##### : ' + caseQuery);
                        CaseAccounts = Database.query(caseQuery);
                        System.debug('final query case : ' + CaseAccounts);
                    }
                    if(CaseAccounts.size()>0){
                        for(Case cs:CaseAccounts){
                             if(String.isNotBlank(cs.Account.Account_ID__c)){
                        		CaseAccountMap.put(cs.Account.Account_ID__c,cs);
                                 if(String.isNotEmpty(InQueryClause)){
                                 	InQueryClause = InQueryClause+'\''+cs.Account.Account_ID__c+'\',';    
                                 }
                                 else{
                                 	InQueryClause = '('+'\''+cs.Account.Account_ID__c+'\',';    
                                 }
                                 
                             }
                        } 
                        if(String.isNotBlank(InQueryClause)){
                            InQueryClause = InQueryClause.removeEnd(',');
                        	InQueryClause = InQueryClause+')';	    
                        }
                        //String AccountContactRelationQuery = '';
                        //String AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c FROM AccountContactRelation WHERE Account.Account_ID__c '+ ' = \'' + String.escapeSingleQuotes(String.valueOf(CaseAccountIds[0].Account.Account_ID__c)) +'\'';  
                        String AccountContactRelationQuery = 'Select Contact.Name, Contact.Person_ID__c, Account.Account_ID__c, Contact.Phone,  Contact.vlocity_cmt__DriversLicenseNumber__c, Contact.recordtype.name,Contact.vlocity_cmt__TaxId__c FROM AccountContactRelation WHERE Account.Account_ID__c IN '+ InQueryClause ;
                        System.debug('final query : ' + AccountContactRelationQuery);
                        if(!CaseAccountMap.isEmpty()){
                        	data = Database.query(AccountContactRelationQuery);    
                        }                                            	    
                    }
                }
                
               //for(Contact con : Contacts)
               if(data != null) {
                    for(AccountContactRelation con : data) {
                        for(String cs:AccountIdMap.get(con.Account.Account_ID__c)){
                            System.debug('st==='+ st);
                        caseNumber = CaseAccountMap.get(con.Account.Account_ID__c).CaseNumber;
                        caseStatus = CaseAccountMap.get(con.Account.Account_ID__c).status;
                        System.debug('Sobjet==='+ con);
                        Map<String, object> resultValueMap = new Map<String, object>();
                        resultValueMap.put('CaseId',caseNumber != null ? caseNumber : '');
                        resultValueMap.put('CaseStatus',caseStatus != null ? caseStatus : '');
                        resultValueMap.put('PersonName',con.Contact.Name != null ? con.Contact.Name : '');
                        resultValueMap.put('PersonId',con.Contact.Person_ID__c != null ? con.Contact.Person_ID__c : '');
                        resultValueMap.put('AccountId',con.Account.Account_ID__c != null ? con.Account.Account_ID__c : '');
                        
                        if(con.Contact.recordtype.name=='Person'){
                            if(!dynamicPersonLableAdded){
                                resultFieldList.add('vlocity_cmt__DriversLicenseNumber__c');
                                
                                newType1 = new Map<String, String> ();
                                newType1.put('label', 'Driver\'s License Number');
                                newType1.put('datatype','text');            
                                resultFieldsLabelTypeMap.put('vlocity_cmt__DriversLicenseNumber__c', newType1);
                                dynamicPersonLableAdded = true;
                            }
                            resultValueMap.put('vlocity_cmt__TaxId__c',''); 
                        	resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c',con.Contact.vlocity_cmt__DriversLicenseNumber__c != null ? con.Contact.vlocity_cmt__DriversLicenseNumber__c : '');    
                        }
                        else if(con.Contact.recordtype.name=='Business'){
                            if(!dynamicBusinessLableAdded){
                                resultFieldList.add('vlocity_cmt__TaxId__c');
                                
                                newType1 = new Map<String, String> ();
                                newType1.put('label', 'TIN');
                                newType1.put('datatype','text');            
                                resultFieldsLabelTypeMap.put('vlocity_cmt__TaxId__c', newType1);
                                dynamicBusinessLableAdded = true;
                            }
                            resultValueMap.put('vlocity_cmt__DriversLicenseNumber__c','');    

                        	resultValueMap.put('vlocity_cmt__TaxId__c',con.Contact.vlocity_cmt__TaxId__c != null ? con.Contact.vlocity_cmt__TaxId__c : '');    
                        }
                        
                        String recordId = con.Contact.Id;                      
                        vlocity_cmt.LookupResult result =new vlocity_cmt.LookupResult(resultFieldsLabelTypeMap,resultFieldList, resultValueMap, recordId, uniqueRequestName, request,verificationResult,verificationFieldList,drBundleName, interactionObjName);                        
                        result.setOptionalVerificationFieldList(optionalVerificationFieldList);                   
                        results.add(result);
                    	}
               		}
                }*/
              }
                
            if((pageSize > 0) && (results.size() > pageSize))
            {
                List<vlocity_cmt.LookupResult> resultSubSet =          
                new List<vlocity_cmt.LookupResult>();
                Integer startIndex = 0;
            
                if(String.isNotBlank(lastRecordId))
                {
                    for(Integer i = 0; i< results.size(); i++)
                      {
                            if(results[i].recordId == lastRecordId)
                            {
                                  startIndex = i + 1;
                            }
                        }
                }
                
                Integer j = 0;
            
                for(j = startIndex; (j<results.size()) && (j < startIndex + pageSize); j++)
                {
                    resultSubset.add(results[j]);
                }
            
                if(j < results.size())
                {
                    hasMore = true;
                }            
                output.put('lookupResults', resultSubset);                
                System.debug('lookupResults :: ' + resultSubset);
                
                if(hasMore)
                {
                        output.put('hasMore', hasMore);
                }
                
                // End R15 pagination support
            }
            else
            {
                    output.put('lookupResults', results);
            }
            System.debug('lookupResults : ' + results);
        }
    
     global void getSearchRequest(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
     {
         System.debug('ino getSearchRequest inputscase : ' + inputs);   
         System.debug('ino getSearchRequest outputcase : ' + output);
         System.debug('ino getSearchRequest optionscase : ' + options);     
         vlocity_cmt.LookupRequest request = (vlocity_cmt.LookupRequest) inputs.get('lookupRequest'); 
         List<Map<String, Object>> previousSearchResults =  request.previousSearchResults;
         Map<String, Object> additionalData = request.additionalData;
         List<String> searchFieldList = request.searchFieldList;
         searchFieldList.add('CaseNumber');
         Map<String, Map<String, String>>labelTypeMap =  request.searchFieldsLabelTypeMap; 
         // newType2
         Map<String, String> newType2 = new Map<String, String> ();
         newType2.put('label','Case Id');
         newType2.put('datatype','text');
         labelTypeMap.put('CaseNumber',newType2);        
         
         Map<String, Object> valueMap = request.searchValueMap;
         System.debug(' LookupRequest is '+request); 
         output.put('lookupRequest', request);
       
    }
    
    global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options)
    {  
        if (methodName == 'getSearchRequest') {
            getSearchRequest(inputs,output,options);   
        } 
        
        else if (methodName == 'getSearchResults')
        {
            getSearchResults(inputs,output,options);
        }
            
       // getSearchResults(methodName,output,options);
       Boolean success = true;     
       return success;
    }

}